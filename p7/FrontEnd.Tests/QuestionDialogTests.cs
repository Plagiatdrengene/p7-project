﻿using System;
using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using Common.Protos.Utility;
using FrontEnd.Pages;
using FrontEnd.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Telerik.JustMock;
using static System.String;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests {
	public class QuestionDialogTests {
		private TestContext _ctx;
		private IQuestionService _questionServiceMock;
		private IRenderedComponent<QuestionDialog> _cut;

		[SetUp]
		public void Setup() {
			// Common code before each test
			_ctx = new TestContext();
			_ctx.Services.AddTestAuthorization();
			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();
			_questionServiceMock = Mock.Create<IQuestionService>();
			_ctx.Services.AddSingleton(_questionServiceMock);
			_cut = _ctx.RenderComponent<QuestionDialog>();
		}

		[Test]
		public void TextFieldsInputInQuestionObject() {
			// Arrange

			// Act
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#contentTextField").Change("testDescription");
			_cut.Find("#cancelButton").Click();

			//Assert
			_cut.Instance.Question.Title.MarkupMatches(_cut.Find("#titleTextField").GetAttribute("value"));
			_cut.Instance.Question.Content!.MarkupMatches(_cut.Find("#contentTextField").GetAttribute("value"));
		}
		
		[Test]
		public void LocationTextFieldInputToLocationVariable() {
			// Arrange
		
			// Act
			_cut.Find("#locationTextField").Change("testLocation");
		
			// Assert
			_cut.Instance.Location.MarkupMatches(_cut.Find("#locationTextField").GetAttribute("value"));
		}
		
		[Test]
		public void CancelButtonEmptyFields() {
			// Arrange
		
			// Act
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#contentTextField").Change("testDescription");
			_cut.Find("#locationTextField").Change("testLocation");
			_cut.Find("#cancelButton").Click();
			
			// Assert
			Assert.IsEmpty(_cut.Find("#titleTextField").GetAttribute("value"));
			Assert.IsEmpty(_cut.Find("#contentTextField").GetAttribute("value"));
			Assert.IsEmpty(_cut.Find("#locationTextField").GetAttribute("value"));
		}
		
		[Test]
		public void CancelButtonClearQuestionObject() {
			// Arrange
		
			// Act
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#contentTextField").Change("testDescription");
			_cut.Find("#locationTextField").Change("testLocation");
			_cut.Find("#cancelButton").Click();
			
			// Assert
			Assert.True(IsNullOrEmpty(_cut.Instance.Question.Title));
			Assert.True(IsNullOrEmpty(_cut.Instance.Question.Content));
			Assert.True(IsNullOrEmpty(_cut.Instance.Location));
		}
		
		[Test]
		public void CancelButtonMakesDialogClose() {
			// Arrange
		
			// Act
			_cut.Instance.IsDialogOpen = true;
			_cut.Find("#cancelButton").Click();
			
			// Assert
			Assert.False(_cut.Instance.IsDialogOpen);
		}
		
		[Test]
		public void CanNotSubmitWithoutTitle() {
			// Arrange
			_cut.Instance.IsDialogOpen = true;
		
			// Act
			_cut.Find("#submitButton").Click();
			
			// Assert
			Assert.True(_cut.Instance.IsDialogOpen);
		}
		
		[Test]
		public void CanSubmitWithJustTitle() {
			// Arrange
			Mock.Arrange( ()=>
				_questionServiceMock.RequestCreateQuestion(Arg.IsAny<Question>())
			).Returns(new RequestResult {Succeeded = true});
			_cut.Instance.IsDialogOpen = true;
		
			// Act
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#submitButton").Click();
			
			// Assert
			Assert.False(_cut.Instance.IsDialogOpen);
		}
		
		[Test]
		public void SubmitButtonEmptyFields() {
			// Arrange
			Mock.Arrange(() => 
				_questionServiceMock.PhysicalQuestionCreationRequest(Arg.IsAny<PhysicalQuestion>(), Arg.AnyInt)
			).Returns(new RequestResult {Succeeded = true});
			_cut.Instance.Question.Poster = new IdentityUser();
			_cut.Instance.Question.OwningSession = new Session("testName", DateTime.Now, DateTime.Now){Id = 1};
		
			// Act
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#contentTextField").Change("testDescription");
			_cut.Find("#locationTextField").Change("testLocation");
			_cut.Find("#submitButton").Click();
			
			// Assert
			Assert.IsEmpty(_cut.Find("#titleTextField").GetAttribute("value"));
			Assert.IsEmpty(_cut.Find("#contentTextField").GetAttribute("value"));
			Assert.IsEmpty(_cut.Find("#locationTextField").GetAttribute("value"));
		}
		
		[Test]
		public void SubmitButtonClearQuestionObjectAndLocation() {
			// Arrange
			Mock.Arrange(() => 
				_questionServiceMock.PhysicalQuestionCreationRequest(Arg.IsAny<PhysicalQuestion>(), Arg.AnyInt)
			).Returns(new RequestResult {Succeeded = true});
			_cut.Instance.Question.Poster = new IdentityUser();
			_cut.Instance.Question.OwningSession = new Session("testName", DateTime.Now, DateTime.Now){Id = 1};
		
			// Act
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#contentTextField").Change("testDescription");
			_cut.Find("#locationTextField").Change("testLocation");
			_cut.Find("#submitButton").Click();
			
			// Assert
			Assert.True(IsNullOrEmpty(_cut.Instance.Question.Title));
			Assert.True(IsNullOrEmpty(_cut.Instance.Question.Content));
			Assert.True(IsNullOrEmpty(_cut.Instance.Location));
		}
		
		[Test]
		public void DoesNotCloseDialogIfSubmitDidNotSucceed() {
			// Arrange
			Mock.Arrange(() => 
				_questionServiceMock.PhysicalQuestionCreationRequest(Arg.IsAny<PhysicalQuestion>(), Arg.AnyInt)
			).Returns(new RequestResult {Succeeded = false});
			_cut.Instance.IsDialogOpen = true;
		
			// Act
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#locationTextField").Change("testLocation");
			_cut.Find("#submitButton").Click();
			
			// Assert
			Assert.True(_cut.Instance.IsDialogOpen);
		}
		
		[Test]
		public void DoesNotClearFieldsIfSubmitDidNotSucceed() {
			// Arrange
			Mock.Arrange(() => 
				_questionServiceMock.PhysicalQuestionCreationRequest(Arg.IsAny<PhysicalQuestion>(), Arg.AnyInt)
			).Returns(new RequestResult {Succeeded = false});
			_cut.Instance.IsDialogOpen = true;
			var title = "testTitle";
			var content = "testDescription";
			var location = "testLocation";
		
			// Act
			_cut.Find("#titleTextField").Change(title);
			_cut.Find("#contentTextField").Change(content);
			_cut.Find("#locationTextField").Change(location);
			_cut.Find("#submitButton").Click();
			
			// Assert
			_cut.Find("#titleTextField").GetAttribute("value").MarkupMatches(title);
			_cut.Find("#contentTextField").GetAttribute("value").MarkupMatches(content);
			_cut.Find("#locationTextField").GetAttribute("value").MarkupMatches(location);
		}
		
		[Test]
		public void DoesNotClearQuestionObjectIfSubmitDidNotSucceed() {
			// Arrange
			Mock.Arrange(() => 
				_questionServiceMock.PhysicalQuestionCreationRequest(Arg.IsAny<PhysicalQuestion>(), Arg.AnyInt)
			).Returns(new RequestResult {Succeeded = false});
			_cut.Instance.IsDialogOpen = true;
			var _title = "testTitle";
			var _content = "testDescription";
			var _location = "testLocation";
		
			// Act
			_cut.Find("#titleTextField").Change(_title);
			_cut.Find("#contentTextField").Change(_content);
			_cut.Find("#locationTextField").Change(_location);
			_cut.Find("#submitButton").Click();
			
			// Assert
			_cut.Instance.Question.Title.MarkupMatches(_title);
			_cut.Instance.Question.Content!.MarkupMatches(_content);
			_cut.Instance.Location.MarkupMatches(_location);
		}
		
		[Test]
		public void CanSubmitWithOutDescription() {
			// Arrange
			Mock.Arrange(() => 
				_questionServiceMock.PhysicalQuestionCreationRequest(Arg.IsAny<PhysicalQuestion>(), Arg.AnyInt)
			).Returns(new RequestResult {Succeeded = true});
			_cut.Instance.Question.Poster = new IdentityUser();
			_cut.Instance.Question.OwningSession = new Session("testName", DateTime.Now, DateTime.Now){Id = 1};
		
			// Act
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#locationTextField").Change("testLocation");
			_cut.Find("#submitButton").Click();
			
			// Assert
			Assert.False(_cut.Instance.IsDialogOpen);
		}

		[Test]
		public void CanSubmitWithOutLocation() {
			// Arrange
			Mock.Arrange( ()=>
				_questionServiceMock.RequestCreateQuestion(Arg.IsAny<Question>())
			).Returns(new RequestResult {Succeeded = true});
			
			// Act
			_cut.Instance.Question.Poster = new IdentityUser();
			_cut.Instance.Question.OwningSession = new Session("testName", DateTime.Now, DateTime.Now){Id = 1};
			
			_cut.Find("#titleTextField").Change("testTitle");
			_cut.Find("#submitButton").Click();
			
			// Assert
			Assert.False(_cut.Instance.IsDialogOpen);
		}

	}
}