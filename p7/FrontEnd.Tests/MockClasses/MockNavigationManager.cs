﻿using Microsoft.AspNetCore.Components;

namespace FrontEnd.Tests.MockClasses {
	//This is a mock of the Navigation manager, required to run tests on components that uses a NavigationManager.
	//Current implementation makes it possible to check if navigation happened.
	internal class MockNavigationManager : NavigationManager
	{
		public bool WasNavigateInvoked { get; private set; }
		public MockNavigationManager()
		{
			//The value in the parameters are not particularly important, as long as "baseUri" is a prefix in "uri".
			Initialize("https://unit-test.example/", "https://unit-test.example/test");
		}

		protected override void NavigateToCore(string uri, bool forceLoad)
		{
			WasNavigateInvoked = true;
		}
	}
}