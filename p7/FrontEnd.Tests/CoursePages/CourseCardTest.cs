﻿using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using FrontEnd.Pages.CoursePages;
using FrontEnd.Services;
using FrontEnd.Tests.MockClasses;
using FrontEnd.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Telerik.JustMock;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests.CoursePages {
	public class CourseCardTest {
		//Test Context
		private TestContext _ctx;
		
		//The services required by the page
		private IRoleService _roleServiceMock;
		private MockNavigationManager _navigationManagerMock;

		//Mocks of ViewModels
		private TinyCourseViewModel _viewModelMock;

		//Used for rendering the component
		private IRenderedComponent<CourseCard> _cut;
		
		//Mock Course
		private TinyCourse _mockCourse;

		[SetUp]
		public void Setup() {
			_ctx = new TestContext();
			
			_ctx.Services.AddTestAuthorization();

			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();
			
			//Create Mock services
			_navigationManagerMock = new MockNavigationManager();
			_ctx.Services.AddSingleton<NavigationManager>(_navigationManagerMock);

			_roleServiceMock = Mock.Create<IRoleService>();
			_ctx.Services.AddSingleton(_roleServiceMock);
			
			//Create Mock ViewModels
			_viewModelMock = Mock.Create<TinyCourseViewModel>();
			_ctx.Services.AddSingleton(_viewModelMock);

			//Create a mock Course
			_mockCourse = new TinyCourse("Name of course", 2077, false, new IdentityUser(), "not a good subtitle");
			_mockCourse.Tags.Add(new Tag("tag1"));
			_mockCourse.Tags.Add(new Tag("tag2"));
			
			//Arrange the mock methods required to render the component
			Mock.Arrange(
				() => _roleServiceMock.RequestIsUserInRole(Arg.IsAny<IdentityUser>(), Arg.AnyString)
			).Returns(true);
			
			Mock.Arrange(() =>
				_viewModelMock.Course
			).Returns(_mockCourse);
			
			//Render the component
			_cut = _ctx.RenderComponent<CourseCard>(("ViewModel", _viewModelMock), ("User",  new IdentityUser()), ("OnUpdate", new EventCallback()));
		}
		
		[Test]
		public void LeaveButtonOpensDialog() {
			//Arrange
			
			
			//Act
			_cut.Find("#LeaveCourseButton").Click();
			
			//Assert
			Assert.True(_cut.Instance.LeaveCourseDialogOpen);
		}
		
		[Test]
		public void CancelButtonClosesDialog() {
			//Arrange
			
			
			_cut.Find("#LeaveCourseButton").Click();
			_cut.Find("#CancelLeaveCourseButton").Click();
			
			//Assert
			Assert.False(_cut.Instance.LeaveCourseDialogOpen);
		}
		
		[Test]
		public void UserLeavesCourse() {
			//Arrange
			Mock.Arrange(
				() => _viewModelMock.RemoveUserFromCourse(Arg.IsAny<IdentityUser>())
			).Returns(true);
			
			//Act
			_cut.Find("#LeaveCourseButton").Click();
			_cut.Find("#YesLeaveCourseButton").Click();
			
			//Assert
			Assert.False(_cut.Instance.LeaveCourseDialogOpen);
			Assert.True(_navigationManagerMock.WasNavigateInvoked);
		}
		
		[Test]
		public void CourseHasCorrectName() {
			//Arrange
			string expectedName = _mockCourse.Name + " (" + (_mockCourse.IsFall ? "Fall " : "Spring ") + _mockCourse.Year + ")";

			//Act
		
			
			//Assert
			_cut.Find("#CourseName").TextContent.MarkupMatches(expectedName);
		}
		
		[Test]
		public void CourseHasCorrectSubTitle() {
			//Arrange
		
			
			//Act
			
			
			//Assert
			_cut.Find("#CourseSubTitle").TextContent.MarkupMatches(_mockCourse.SubTitle);
		}
		
		[Test]
		public void CourseHasCorrectTags() {
			//Arrange
			var i = 0;
			var allTags = _cut.FindAll("#Tags");
			
			//Act
			
			
			//Assert
			Assert.AreEqual(_mockCourse.Tags.Count, allTags.Count);
			while ( i < _mockCourse.Tags.Count) {
				allTags[i].TextContent.MarkupMatches(_mockCourse.Tags[i].TagName);
				i++;
			}
		}
	}
}