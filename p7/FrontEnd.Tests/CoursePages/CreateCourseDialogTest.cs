﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using FrontEnd.Pages.CoursePages;
using FrontEnd.Services;
using FrontEnd.Tests.MockClasses;
using FrontEnd.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Telerik.JustMock;
using Course = Common.Data.Course;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests.CoursePages {
	public class CreateCourseDialogTest {
		//Test Context
		private TestContext _ctx;
		
		//The services required by the page
		private ICourseService _courseServiceMock;
		private UserManager<IdentityUser> _userManagerMock;
		private RoleManager<IdentityRole> _roleManagerMock;
		private MockNavigationManager _navigationManagerMock;
		
		//Mocks of ViewModels
		private TinyCourseViewModel _viewModelMock;
		
		//Used for rendering the component
		private IRenderedComponent<CreateCourseDialog> _cut;
		
		//Mock Course
		private TinyCourse _mockCourse;

		[SetUp]
		public void Setup() {
			_ctx = new TestContext();
			
			var authContext = _ctx.Services.AddTestAuthorization();
			authContext.SetAuthorized("1", AuthorizationState.Authorized);
			authContext.SetRoles("Staff");
			
			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();
			
			//Create Mock services
			_courseServiceMock = Mock.Create<ICourseService>();
			_ctx.Services.AddSingleton(_courseServiceMock);
			
			_userManagerMock = Mock.Create<UserManager<IdentityUser>>();
			_ctx.Services.AddSingleton(_userManagerMock);

			_roleManagerMock = Mock.Create<RoleManager<IdentityRole>>();
			_ctx.Services.AddSingleton(_roleManagerMock);

			_navigationManagerMock = new MockNavigationManager();
			_ctx.Services.AddSingleton<NavigationManager>(_navigationManagerMock);

			//Create Mock ViewModels
			_viewModelMock = Mock.Create<TinyCourseViewModel>();
			_ctx.Services.AddSingleton(_viewModelMock);
			
			//Create mock Course
			_mockCourse = new Course();

			//Arrange the mock methods required to render the component
			Mock.Arrange(
				() => _viewModelMock.Course
			).Returns(_mockCourse);

			_cut = _ctx.RenderComponent<CreateCourseDialog>();
		}

		[Test]
		public void CreateCourseOnValidSubmit() {
			//Arrange
			Mock.Arrange(
				() => _courseServiceMock.CreateCourse(_viewModelMock.Course)
			).Returns("MYCODE");
			
			Mock.Arrange(
				() => _roleManagerMock.CreateAsync(Arg.IsAny<IdentityRole>())
			).Returns(Task.FromResult(new IdentityResult()));

			Mock.Arrange(
				() => _userManagerMock.AddToRoleAsync(Arg.IsAny<IdentityUser>(), Arg.AnyString)
			).Returns(Task.FromResult(new IdentityResult()));
			
			//Test variables
			string courseName = "This is a Course";
			string courseSubtitle = "This is a Subtitle";
			int courseYear = 2000;
			bool courseIsFall = true;
			string courseTags = "tag1, tag2";
			string courseLanguages = "lang1, lang2";
			
			//Used when asserting tags/languages
			List<Tag> tags = courseTags.Split(",").Select(tagName => new Tag(tagName.Trim())).ToList();
			List<Language> languages = courseLanguages.Split(",").Select(lang => new Language(lang.Trim())).ToList();
			
			//Loop variables
			int i = 0;
			int j = 0;
			
			//Act
			_cut.Find("#CourseNameField").Change(courseName);
			_cut.Find("#CourseSubtitleField").Change(courseSubtitle);
			_cut.Find("#CourseYearField").Change(courseYear.ToString());
			_cut.Find("#CourseSeasonCheckbox").Change(courseIsFall);
			_cut.Find("#CourseTagsField").Change(courseTags);
			_cut.Find("#CourseLanguageField").Change(courseLanguages);
			_cut.Find("form").Submit();


			//Assert
			Assert.True(_navigationManagerMock.WasNavigateInvoked);
			_viewModelMock.Course.Name.MarkupMatches(courseName);
			_viewModelMock.Course.SubTitle.MarkupMatches(courseSubtitle);
			Assert.AreEqual(courseYear, _viewModelMock.Course.Year);
			Assert.AreEqual(courseIsFall, _viewModelMock.Course.IsFall);
			Assert.AreEqual(tags.Count, _viewModelMock.Course.Tags.Count);
			while (i < tags.Count) {
				_viewModelMock.Course.Tags[i].TagName.MarkupMatches(tags[i].TagName);
				i++;
			}
			Assert.AreEqual(languages.Count, _viewModelMock.Course.SupportedLanguages.Count);
			while (j < languages.Count) {
				_viewModelMock.Course.SupportedLanguages[j].LanguageName.MarkupMatches(languages[j].LanguageName);
				j++;
			}
		}
		
		[Test]
		public void CancelButtonClosesDialog() {
			//Arrange
			
			
			//Act
			_cut.Find("#CancelButton").Click();

			//Assert
			Assert.False(_cut.Instance.DialogIsOpen);
		}
		
		[Test]
		public void CancelButtonResetsCourseVariables() {
			//Arrange
			
			
			//Act
			_cut.Find("#CourseNameField").Change("new Name");
			_cut.Find("#CourseSubtitleField").Change("New subtitle");
			_cut.Find("#CourseYearField").Change("1500");
			_cut.Find("#CourseSeasonCheckbox").Change(true);
			_cut.Find("#CourseTagsField").Change("tag1");
			_cut.Find("#CourseLanguageField").Change("Danish");
			_cut.Find("#CancelButton").Click();

			//Assert
			Assert.IsEmpty(_viewModelMock.Course.Name);
			Assert.IsEmpty(_viewModelMock.Course.SubTitle);
			Assert.AreEqual(DateTime.Now.Year, _viewModelMock.Course.Year);
			Assert.False(_viewModelMock.Course.IsFall);
			Assert.IsEmpty(_viewModelMock.Course.Tags);
			Assert.IsEmpty(_viewModelMock.Course.SupportedLanguages);
		}
	}
}