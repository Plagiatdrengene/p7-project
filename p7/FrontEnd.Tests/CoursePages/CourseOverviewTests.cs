﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using Common.Protos.Utility;
using FrontEnd.Pages;
using FrontEnd.Pages.CoursePages;
using FrontEnd.Services;
using FrontEnd.Tests.MockClasses;
using FrontEnd.ViewModels;
using MatBlazor;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Telerik.JustMock;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests.CoursePages {
	public class CourseOverviewTests {
		//Test Context
		private TestContext _ctx;

		//The services required by the page
		private ICourseService _courseServiceMock;
		private IRoleService _roleServiceMock;
		private UserManager<IdentityUser> _userManagerMock;
		private RoleManager<IdentityRole> _roleManagerMock;
		private MockNavigationManager _navigationManagerMock;
		private IMatToaster _toasterMock;

		//Mocks of ViewModels
		private TinyCourseViewModel _viewModelMock;

		//Used for rendering the component
		private IRenderedComponent<Index> _cut;

		//Mock user
		private IdentityUser _mockUser;

		//List of mock courses
		private List<TinyCourse> _courses;

		[SetUp]
		public void Setup() {
			_ctx = new TestContext();

			TestAuthorizationContext authContext = _ctx.Services.AddTestAuthorization();
			authContext.SetAuthorized("1", AuthorizationState.Authorized);
			authContext.SetRoles("Staff");

			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();

			//Create Mock services
			_courseServiceMock = Mock.Create<ICourseService>();
			_ctx.Services.AddSingleton(_courseServiceMock);

			_userManagerMock = Mock.Create<UserManager<IdentityUser>>();
			_ctx.Services.AddSingleton(_userManagerMock);

			_roleServiceMock = Mock.Create<IRoleService>();
			_ctx.Services.AddSingleton(_roleServiceMock);
			
			_roleManagerMock = Mock.Create<RoleManager<IdentityRole>>();
			_ctx.Services.AddSingleton(_roleManagerMock);

			_navigationManagerMock = new MockNavigationManager();
			_ctx.Services.AddSingleton<NavigationManager>(_navigationManagerMock);
			
			_toasterMock = Mock.Create<IMatToaster>();
			_ctx.Services.AddSingleton(_toasterMock);

			//Create Mock ViewModels
			_viewModelMock = Mock.Create<TinyCourseViewModel>();
			_ctx.Services.AddSingleton(_viewModelMock);

			//Create a mock user
			_mockUser = new IdentityUser {Id = "1", Email = "2"};
			//Create mock courses
			_courses = new List<TinyCourse>{new TinyCourse("TestCourse", 2020, true, _mockUser, "MySubtitle")};

			//Arrange the mock methods required to render the component
			Mock.Arrange(() =>
				_courseServiceMock.GetAllCoursesThatUserIsPartOf(Arg.AnyString)
			).Returns(_courses);

			Mock.Arrange(
				() => _userManagerMock.GetUserAsync(Arg.IsAny<ClaimsPrincipal>())
			).Returns(Task.FromResult(_mockUser));

			Mock.Arrange(() =>
				_viewModelMock.Course
			).Returns(new TinyCourse("Name of course", 2077, false, _mockUser, "not a good subtitle"));

			//Render the component
			_cut = _ctx.RenderComponent<Index>();
		}

		[Test]
		public void JoinButtonOpensDialog() {
			//Arrange


			//Act
			_cut.Find("#JoinCourseButton").Click();

			//Assert
			Assert.True(_cut.Instance.JoinCourseDialogOpen);
		}

		[Test]
		public void TextFieldsInputInCourseCodeObject() {
			// Arrange

			// Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#CourseCodeField").Change("CODEF");

			// Assert
			_cut.Instance.JoinCourseCode.MarkupMatches(_cut.Find("#CourseCodeField").GetAttribute("value"));
		}

		[Test]
		public void CancelButtonEmptyField() {
			// Arrange

			// Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#CourseCodeField").Change("CODEF");
			_cut.Find("#CancelJoinCourseButton").Click();

			// Assert
			Assert.IsEmpty(_cut.Find("#CourseCodeField").GetAttribute("value"));
		}

		[Test]
		public void CancelButtonEmptyJoinCourseCode() {
			// Arrange

			// Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#CourseCodeField").Change("CODEF");
			_cut.Find("#CancelJoinCourseButton").Click();

			// Assert
			Assert.IsEmpty(_cut.Instance.JoinCourseCode);
		}

		[Test]
		public void CancelButtonClosesDialog() {
			// Arrange

			// Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#CancelJoinCourseButton").Click();

			// Assert
			Assert.False(_cut.Instance.JoinCourseDialogOpen);
		}

		[Test]
		public void JoinCourseWithValidCourseCode() {
			//Arrange
			const string courseCode = "TESTC";

			Mock.Arrange(() =>
				_courseServiceMock.JoinCourse(courseCode, _mockUser.Id)
			).Returns(() => {
				_courses.Add(new TinyCourse(courseCode, "Test Course", 2077, true, _mockUser));
				return new RequestResult {Succeeded = true};
			});

			//Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#CourseCodeField").Change(courseCode);
			_cut.Find("#JoinCourseWithCodeButton").Click();

			//Assert
			Assert.False(_cut.Instance.JoinCourseDialogOpen);
			Assert.True(_navigationManagerMock.WasNavigateInvoked);
			Assert.AreEqual(2, _cut.FindComponents<CourseCard>().Count);
		}

		[Test]
		public void JoinCourseCodeEmptyFails() {
			//Arrange


			//Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#JoinCourseWithCodeButton").Click();

			//Assert
			Assert.True(_cut.Instance.JoinCourseDialogOpen);
			_cut.Instance.JoinCourseErrors.MarkupMatches("Code must be between 4 and 6 characters");
		}

		[Test]
		public void JoinCourseCodeTooShortFails() {
			//Arrange


			//Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#CourseCodeField").Change("Bad");
			_cut.Find("#JoinCourseWithCodeButton").Click();

			//Assert
			Assert.True(_cut.Instance.JoinCourseDialogOpen);
			_cut.Instance.JoinCourseErrors.MarkupMatches("Code must be between 4 and 6 characters");
		}

		[Test]
		public void JoinCourseCodeTooLongFails() {
			//Arrange

			//Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#CourseCodeField").Change("TOOLONG");
			_cut.Find("#JoinCourseWithCodeButton").Click();

			//Assert
			Assert.True(_cut.Instance.JoinCourseDialogOpen);
			_cut.Instance.JoinCourseErrors.MarkupMatches("Code must be between 4 and 6 characters");
		}

		[Test]
		public void CancelButtonEmptyJoinCourseErrors() {
			// Arrange

			// Act
			_cut.Find("#JoinCourseButton").Click();
			_cut.Find("#CourseCodeField").Change("TOOLONG");
			_cut.Find("#JoinCourseWithCodeButton").Click();
			_cut.Find("#CancelJoinCourseButton").Click();

			// Assert
			Assert.IsEmpty(_cut.Instance.JoinCourseErrors);
		}

		[Test]
		public void CreateCourseButtonOpensDialog() {
			//Arrange


			//Act
			_cut.Find("#CreateCourseButton").Click();

			//Assert
			Assert.True(_cut.Instance.CreateCourse.DialogIsOpen);
		}
	}
}