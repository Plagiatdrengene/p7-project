﻿using Bunit;
using Bunit.TestDoubles.Authorization;
using Bunit.TestDoubles.JSInterop;
using Common.Data;
using FrontEnd.Pages.QuestionPages;
using FrontEnd.Tests.MockClasses;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using TestContext = Bunit.TestContext;

namespace FrontEnd.Tests {
	public class QuestionCardTest {
		//Test Context
		private TestContext _ctx;
		
		//Mock Question
		private Question _mockQuestion;
		
		//Used for rendering the component
		private IRenderedComponent<QuestionCard> _cut;

		[SetUp]
		public void Setup() {
			_ctx = new TestContext();
			_ctx.Services.AddTestAuthorization();
			_ctx.Services.AddRazorPages();
			_ctx.Services.AddMockJSRuntime();
			
			//Create Mock Services
			_ctx.Services.AddSingleton<NavigationManager>(new MockNavigationManager());
			
			//Create Mock Question
			_mockQuestion = new Question(new IdentityUser(), "My Question", "My Content");
			
			//Render the component
			_cut = _ctx.RenderComponent<QuestionCard>(("Question", _mockQuestion), ("CourseCode", "MYCODE"));
		}

		[Test]
		public void QuestionHasCorrectTitle() {
			//Arrange
		
			
			//Act
			
			
			//Assert
			_cut.Find("#QuestionTitle").TextContent.MarkupMatches(_mockQuestion.Title);
		}
		
		[Test]
		public void QuestionHasCorrectContent() {
			//Arrange
		
			
			//Act
			
			
			//Assert
			_cut.Find("#QuestionContent").TextContent.MarkupMatches(_mockQuestion.Content!);
		}
	}
}