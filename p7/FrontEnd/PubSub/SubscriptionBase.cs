﻿using System;
using System.Text;
using Common.PubSub;
using Grpc.Core;
using KubeMQ.SDK.csharp.Events;
using KubeMQ.SDK.csharp.Subscription;
using KubeMQ.SDK.csharp.Tools;

namespace FrontEnd.PubSub {
	public abstract class SubscriptionBase {
		public readonly PubSubInformation PubSubInformation;
		private Subscriber Subscriber { get; }

		protected SubscriptionBase(string channelName, string clientId, string kubeMqServerAddress) {
			PubSubInformation = new PubSubInformation(channelName, clientId, kubeMqServerAddress);
			if (!Startup.IsContainerized) return;

			Subscriber = new Subscriber(PubSubInformation.KubeMqServerAddress);
			try {
				Subscriber.SubscribeToEvents(new SubscribeRequest {
						Channel = PubSubInformation.ChannelName,
						SubscribeType = SubscribeType.Events,
						ClientID = PubSubInformation.ClientId
					},
					eventReceive => { OnEvent(Encoding.Default.GetString(eventReceive.Body)); },
					errorHandler => { Console.Error.WriteLine(errorHandler.Message); });
			}
			catch (RpcException ex) {
				Console.Error.WriteLine("Failed at subscribing  printing error message:\n" + ex.Message);
			}
		}

		protected abstract void OnEvent(string eventReceiveBodyJson);
	}
}