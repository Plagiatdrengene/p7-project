﻿using System.Data;
using Common.PubSub.Data;
using Newtonsoft.Json;

namespace FrontEnd.PubSub {
	public class CommentSubscription : SubscriptionBase {
		public readonly PostableEvent PostableEvent = new();

		public CommentSubscription() : base("Comment", "CommentFrontend",
			"kubemq-cluster-grpc.kubemq.svc.cluster.local:50000") { }

		protected override void OnEvent(string eventReceiveBodyJson) {
			var update = JsonConvert.DeserializeObject<PostableUpdate>(eventReceiveBodyJson);
			if (update is null)
				throw new NoNullAllowedException();
			PostableEvent.FireEvent(update);
		}
	}
}