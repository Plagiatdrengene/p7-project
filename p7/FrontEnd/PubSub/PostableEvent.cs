﻿using Common.PubSub.Data;

namespace FrontEnd.PubSub {
	public class PostableEvent {
		public delegate void UpdatePostableEvent(PostableUpdate postableEvent);

		private event UpdatePostableEvent OnPostableEvent;

		public void Subscribe(UpdatePostableEvent updateEvent) {
			OnPostableEvent += updateEvent;
		}

		public void UnSubscribe(UpdatePostableEvent updateEvent) {
			OnPostableEvent -= updateEvent;
		}

		public void FireEvent(PostableUpdate postableEvent) {
			OnPostableEvent?.Invoke(postableEvent);
		}
	}
}