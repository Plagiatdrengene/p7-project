﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Data;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Pages.CoursePages {
	public partial class CreateCourseDialog {
		public bool DialogIsOpen = false;
		private string _langs = "";
		private List<Language> Languages => _langs.Split(",").Select(lang => new Language(lang.Trim())).ToList();
		private string _tags = "";
		private List<Tag> Tags => _tags.Split(",").Select(tagName => new Tag(tagName.Trim())).ToList();
		private IdentityUser _user;

		private async Task HandleValidSubmit() {
			ViewModel.Course.Tags = Tags;
			ViewModel.Course.SupportedLanguages = Languages;
			DialogIsOpen = false;
			string courseCode = CourseService.CreateCourse(ViewModel.Course);
			await RoleManager.CreateAsync(new IdentityRole("Lecturer-" + courseCode));
			await UserManager.AddToRoleAsync(_user, "Lecturer-" + courseCode);
			NavigationManager.NavigateTo("api/RefreshSignIn/Course-" + courseCode, true);
		}

		protected override async Task OnInitializedAsync() {
			await base.OnInitializedAsync();
			AuthenticationState state = await StateProvider.GetAuthenticationStateAsync();
			if (state.User == null) {
				return;
			}

			_user = await UserManager.GetUserAsync(state.User);
			if (_user == null) return;
			ViewModel.Course.Creator = _user;
			ViewModel.Course.Year = DateTime.Now.Year;
		}

		public void Show() {
			DialogIsOpen = true;
			StateHasChanged();
		}

		public void Hide() {
			DialogIsOpen = false;
			//Return all fields to their initial state
			ViewModel.Course.Name = String.Empty;
			ViewModel.Course.SubTitle = String.Empty;
			ViewModel.Course.Year = DateTime.Now.Year;
			ViewModel.Course.IsFall = false;
			_tags = String.Empty;
			ViewModel.Course.Tags = new List<Tag>();
			_langs = String.Empty;
			ViewModel.Course.SupportedLanguages = new List<Language>();
			StateHasChanged();
		}
	}
}