﻿using System.Threading.Tasks;
using FrontEnd.Pages.CoursePages.Components;
using FrontEnd.ViewModels;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Pages.Shared {
	public partial class CourseLayout {
		public string CourseCode { get; set; }
		public IdentityUser User;
		public TinyCourseViewModel ViewModel;
		

		protected override void OnInitialized() {
			object courseCode = null;
			if ((Body.Target as RouteView)?.RouteData.RouteValues?.TryGetValue("CourseCode", out courseCode) == true) {
				CourseCode = courseCode?.ToString();
			}

			if (CourseCode == null) {
				NavigationManager.NavigateTo("/NoCourseSelected");
				return;
				//throw new NotSupportedException(
				//	"The page using this layout needs to have CourseCode as a routing parameter");
			}

			CourseCode = CourseCode.ToUpper();
			ViewModel = new TinyCourseViewModel(CourseCode, QuestionSubscription, NavigationManager);
			base.OnInitialized();
		}

		protected override async Task OnInitializedAsync() {
			AuthenticationState state = await StateProvider.GetAuthenticationStateAsync();
			if (state.User == null) {
				return;
			}

			IdentityUser user = await UserManager.GetUserAsync(state.User);
			if (user == null) {
				return;
			}

			User = user;
		}

		private string GetAllowedRoles =>
			$"Student-{CourseCode}, TeachingAssistant-{CourseCode}, Lecturer-{CourseCode}";
	}
}