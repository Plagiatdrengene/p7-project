﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace FrontEnd {
	public class CustomSignInManager : SignInManager<IdentityUser> {
		public CustomSignInManager(UserManager<IdentityUser> userManager, IHttpContextAccessor contextAccessor,
			IUserClaimsPrincipalFactory<IdentityUser> claimsFactory, IOptions<IdentityOptions> optionsAccessor,
			ILogger<SignInManager<IdentityUser>> logger, IAuthenticationSchemeProvider schemes,
			IUserConfirmation<IdentityUser> confirmation) : base(userManager, contextAccessor, claimsFactory,
			optionsAccessor, logger, schemes, confirmation) { }

		/// <summary>
		/// Attempts to sign in the specified <paramref name="email"/> and <paramref name="password"/> combination
		/// as an asynchronous operation.
		/// </summary>
		/// <param name="email">The user email to sign in.</param>
		/// <param name="password">The password to attempt to sign in with.</param>
		/// <param name="isPersistent">Flag indicating whether the sign-in cookie should persist after the browser is closed.</param>
		/// <param name="lockoutOnFailure">Flag indicating if the user account should be locked if the sign in fails.</param>
		/// <returns>The task object representing the asynchronous operation containing the <see name="SignInResult"/>
		/// for the sign-in attempt.</returns>
		/// <returns>Overrides default behavior for signing in, normally uses username, but has been changed to email</returns>
		public override async Task<SignInResult> PasswordSignInAsync(string email, string password,
			bool isPersistent, bool lockoutOnFailure) {
			IdentityUser user = await UserManager.FindByEmailAsync(email);
			if (user == null) {
				return SignInResult.Failed;
			}

			return await PasswordSignInAsync(user, password, isPersistent, lockoutOnFailure);
		}
	}
}