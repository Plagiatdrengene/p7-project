using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Protos.Claims;
using Common.Protos.Roles;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Protos.Token;
using Common.Utility;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Storage {
	public sealed partial class UserStore : UserStoreBase<IdentityUser, IdentityRole, string, IdentityUserClaim<string>,
		IdentityUserRole<string>, IdentityUserLogin<string>, IdentityUserToken<string>, IdentityRoleClaim<string>> {
		private readonly UserHandler.UserHandlerClient _userClient;
		private readonly TokenHandler.TokenHandlerClient _tokenClient;
		public override IQueryable<IdentityUser> Users => GetAllUsers();

		public UserStore(IdentityErrorDescriber describer) : base(describer) {
			GrpcChannel channel = Startup.CreateStandardNewChannel;
			_userClient = new UserHandler.UserHandlerClient(channel);
			_claimsClient = new ClaimsHandler.ClaimsHandlerClient(channel);
			_roleClient = new RoleHandler.RoleHandlerClient(channel);
			_tokenClient = new TokenHandler.TokenHandlerClient(channel);
		}

		public override async Task<IdentityResult> CreateAsync(IdentityUser user,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult userReply = await _userClient.RequestCreateUserAsync(new UserCreateRequest
					{UserId = user.Id, Email = user.Email, Username = user.UserName, PasswordHash = user.PasswordHash},
				cancellationToken: cancellationToken);
			return userReply.ToIdentityResult();
		}

		public override async Task<IdentityResult> UpdateAsync(IdentityUser user,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult requestResult;
			if (user.PasswordHash != null) {
				requestResult =
					await _userClient.RequestUserUpdateAsync(user.ToUserReply(), cancellationToken: cancellationToken);
			}
			else {
				requestResult =
					await _userClient.RequestUserUpdateSafeAsync(user.ToUserReplySafe(),
						cancellationToken: cancellationToken);
			}

			RequestConversions.CheckForSuccess(requestResult);
			return requestResult.ToIdentityResult();
		}

		public override async Task<IdentityResult> DeleteAsync(IdentityUser user,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult requestResult = await
				_userClient.RequestUserDeletionAsync(new UserRequestById {UserId = user.Id},
					cancellationToken: cancellationToken);
			RequestConversions.CheckForSuccess(requestResult);
			return requestResult.ToIdentityResult();
		}

		public override async Task<IdentityUser> FindByIdAsync(string userId,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			UserReply userReply = await _userClient.RequestUserByIdAsync(new UserRequestById {UserId = userId},
				cancellationToken: cancellationToken);

			return RequestConversions.CheckForSuccess(userReply.Result) ? userReply.ToIdentityUser() : null;
		}

		public override async Task<IdentityUser> FindByNameAsync(string normalizedUserName,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			UserReply userReply = await _userClient.RequestUserByNameAsync(
				new UserRequestByName {UserName = normalizedUserName},
				cancellationToken: cancellationToken);
			return RequestConversions.CheckForSuccess(userReply.Result) ? userReply.ToIdentityUser() : null;
		}

		protected override Task<IdentityUser> FindUserAsync(string userId, CancellationToken cancellationToken) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			UserReply userReply = _userClient.RequestUserById(new UserRequestById {UserId = userId},
				cancellationToken: cancellationToken);
			return Task.FromResult(userReply.ToIdentityUser());
		}

		protected override Task<IdentityUserToken<string>> FindTokenAsync(IdentityUser user, string loginProvider,
			string name, CancellationToken cancellationToken) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			TokenReply tokenReply = _tokenClient.RequestFindToken(new TokenRequestFind {
				TokenUserId = user.Id,
				TokenLoginProvider = loginProvider, TokenName = name
			}, cancellationToken: cancellationToken);
			return Task.FromResult(tokenReply.ToIdentityUserToken());
		}

		protected override async Task AddUserTokenAsync(IdentityUserToken<string> token) {
			ThrowIfDisposed();

			RequestResult result =
				await _tokenClient.RequestAddTokenAsync(new TokenRequest {
					TokenName = token.Name,
					TokenValue = token.Value, TokenLoginProvider = token.LoginProvider, TokenUserId = token.UserId
				});
			RequestConversions.CheckForSuccess(result);
		}

		protected override async Task RemoveUserTokenAsync(IdentityUserToken<string> token) {
			ThrowIfDisposed();

			RequestResult result =
				await _tokenClient.RequestRemoveTokenAsync(new TokenRequest {
					TokenName = token.Name,
					TokenValue = token.Value, TokenLoginProvider = token.LoginProvider, TokenUserId = token.UserId
				});
			RequestConversions.CheckForSuccess(result);
		}


		public override async Task<IdentityUser> FindByEmailAsync(string normalizedEmail,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			UserReply userReply =
				await _userClient.RequestUserByEmailAsync(new UserRequestByEmail {Email = normalizedEmail},
					cancellationToken: cancellationToken);
			return RequestConversions.CheckForSuccess(userReply.Result) ? userReply.ToIdentityUser() : null;
		}

		private async Task<IQueryable<IdentityUser>> GetAllUsersAsync() {
			ThrowIfDisposed();

			UserListReply userReply = await _userClient.RequestAllUsersAsync(new Empty());
			return await Task.FromResult(userReply.Users.Select(RequestConversions.ToIdentityUser).AsQueryable());
		}

		private IQueryable<IdentityUser> GetAllUsers() {
			ThrowIfDisposed();

			UserListReply userReply = _userClient.RequestAllUsers(new Empty());
			return userReply.Users.Select(RequestConversions.ToIdentityUser).AsQueryable();
		}
	}
}