using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.Protos.Roles;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Storage {
	public partial class UserStore {
		private readonly RoleHandler.RoleHandlerClient _roleClient;

		public override async Task<bool> IsInRoleAsync(IdentityUser user, string normalizedRoleName,
			CancellationToken cancellationToken = new()) {
			UserIsInRoleReply isInRole = await _roleClient.RequestIsInRoleAsync(new UserRoleRequestByName
				{NormalizedRoleName = normalizedRoleName, UserId = user.Id}, cancellationToken: cancellationToken);
			return RequestConversions.CheckForSuccess(isInRole.RequestResult) && isInRole.IsInRole;
		}

		protected override async Task<IdentityRole> FindRoleAsync(string normalizedRoleName,
			CancellationToken cancellationToken) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RoleReply role = await _roleClient.RequestRoleByNameAsync(
				new RoleRequestByName() {NormalizedName = normalizedRoleName},
				cancellationToken: cancellationToken);
			if (!RequestConversions.CheckForSuccess(role.RequestResult))
				return null;

			return new IdentityRole() {
				Id = role.Id, Name = role.Name, ConcurrencyStamp = role.ConcurrencyStamp,
				NormalizedName = role.NormalizedName
			};
		}

		protected override async Task<IdentityUserRole<string>> FindUserRoleAsync(string userId, string roleId,
			CancellationToken cancellationToken) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			UserRole role = await _roleClient.RequestUserRoleAsync(
				new UserRoleRequestById {RoleId = roleId, UserId = userId},
				cancellationToken: cancellationToken);
			if (RequestConversions.CheckForSuccess(role.Result))
				return new IdentityUserRole<string> {
					RoleId = role.RoleId, UserId = role.UserId
				};
			return null;
		}

		public override async Task<IList<IdentityUser>> GetUsersInRoleAsync(string normalizedRoleName,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			UserListReply users = await _roleClient.RequestUsersInRoleAsync(
				new RoleRequestByName {NormalizedName = normalizedRoleName},
				cancellationToken: cancellationToken);
			if (users.Users is null || users.Users.Count == 0)
				return new List<IdentityUser>();

			return users.Users.Select(safe => safe.ToIdentityUser()).ToList();
		}

		public override async Task AddToRoleAsync(IdentityUser user, string normalizedRoleName,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult result = await _roleClient.RequestAddToRoleAsync(
				new UserRoleRequestByName {UserId = user.Id, NormalizedRoleName = normalizedRoleName},
				cancellationToken: cancellationToken);
			RequestConversions.CheckForSuccess(result);
		}

		public override async Task RemoveFromRoleAsync(IdentityUser user, string normalizedRoleName,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult result = await _roleClient.RequestRemoveFromRoleAsync(
				new UserRoleRequestByName {UserId = user.Id, NormalizedRoleName = normalizedRoleName},
				cancellationToken: cancellationToken);

			RequestConversions.CheckForSuccess(result);
		}

		public override async Task<IList<string>> GetRolesAsync(IdentityUser user,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RoleNameList roles = await _roleClient.RequestGetRolesAsync(new UserRequestById() {UserId = user.Id},
				cancellationToken: cancellationToken);
			if (roles.Roles is null || roles.Roles.Count == 0)
				return new List<string>();

			return roles.Roles.Select(request => request.Name).ToList();
		}
	}
}