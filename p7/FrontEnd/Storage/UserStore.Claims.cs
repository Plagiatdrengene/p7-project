using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Common.Protos.Claims;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Storage {
	public partial class UserStore {
		private readonly ClaimsHandler.ClaimsHandlerClient _claimsClient;

		public override async Task<IList<Claim>> GetClaimsAsync(IdentityUser user,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			ClaimsReply reply = await _claimsClient.RequestUserClaimsAsync(new UserRequestById {UserId = user.Id},
				cancellationToken: cancellationToken);
			IList<Claim> result = reply.Claims.Select(claim => claim.ToClaim()).ToList();
			return result;
		}

		public override async Task AddClaimsAsync(IdentityUser user, IEnumerable<Claim> claims,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			RequestResult reply = await _claimsClient.RequestAddClaimsAsync(new UserClaimsRequest() {
				User = user.ToUserReplySafe(),
				Claims = {claims.Select(claim => claim.ToProtoClaim())}
			}, cancellationToken: cancellationToken);

			RequestConversions.CheckForSuccess(reply);
		}

		public override async Task ReplaceClaimAsync(IdentityUser user, Claim claim, Claim newClaim,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();
			RequestResult reply = await _claimsClient.RequestReplaceClaimAsync(new ClaimsReplaceRequest() {
				User = user.ToUserReplySafe(),
				NewClaim = newClaim.ToProtoClaim(),
				OldClaim = claim.ToProtoClaim()
			}, cancellationToken: cancellationToken);
			RequestConversions.CheckForSuccess(reply);
		}

		public override async Task RemoveClaimsAsync(IdentityUser user, IEnumerable<Claim> claims,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			RequestResult reply = await _claimsClient.RequestRemoveClaimsAsync(new UserClaimsRequest()
					{User = user.ToUserReplySafe(), Claims = {claims.Select(claim => claim.ToProtoClaim())}},
				cancellationToken: cancellationToken);
			RequestConversions.CheckForSuccess(reply);
		}

		public override async Task<IList<IdentityUser>> GetUsersForClaimAsync(Claim claim,
			CancellationToken cancellationToken = new()) {
			cancellationToken.ThrowIfCancellationRequested();
			ThrowIfDisposed();

			UserListReply reply =
				await _claimsClient.RequestUsersForClaimAsync(claim.ToProtoClaim(),
					cancellationToken: cancellationToken);
			return reply.Users.Select(safe => safe.ToIdentityUser()).ToList();
		}
	}
}