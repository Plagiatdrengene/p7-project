﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Data;
using Common.Protos.PhysicalQuestion;
using Common.Protos.Utility;

namespace FrontEnd.Services {
	public interface IQuestionService  : IDisposable {

		public RequestResult PhysicalQuestionCreationRequest(PhysicalQuestion physicalQuestion, int sessionId);
		public Task<List<PhysicalQuestion>> GetAllPhysicalQuestionsForSession(int sessionId);
		public List<PhysicalQuestion> GetAllPhysicalQuestionsForCourse(string courseId);
		public List<Question> GetSessionQuestionsWithoutAnswers(int sessionId);
		public Task<List<Question>> GetSessionQuestionsWithoutAnswersAsync(int sessionId);
		public RequestResult RequestCreateQuestion(Question question);
		public RequestResultWithId RequestCreateQuestionAnswer(QuestionAnswer questionAnswer);
		public RequestResultWithId RequestCreateComment(Comment comment);
		public Task<Question> GetQuestionById(int id);


	}
}