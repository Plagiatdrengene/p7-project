﻿using System.Collections.Generic;
using Common.Protos.Roles;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Services {
	public interface IRoleService {
		public List<string> RequestRolesForUser(IdentityUser user);
		public IList<IdentityUser> RequestUsersInRoleAsync(IdentityRole role);
		public bool RequestIsUserInRole(IdentityUser user, string role);
		public void RequestAddToRoleAsync(IdentityUser user, string normalizedRoleName);
		public void RequestRemoveFromRoleAsync(IdentityUser user, string normalizedRoleName);
	}
}