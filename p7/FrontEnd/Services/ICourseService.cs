﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Common.Data;
using Common.Protos.Utility;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Services {
	public interface ICourseService {
		public Course RequestCourse(string courseCode);
		public TinyCourse RequestTinyCourse(string courseCode);
		public string CreateCourse(TinyCourse courseToCreate);
		public List<TinyCourse> GetAllCoursesThatUserIsPartOf(string userId);
		public bool IsUserPartOfCourse(string courseId, string userId);
		public bool RemoveUserFromCourse(string courseId, string userId);
		public RequestResult JoinCourse(string courseCode, string userId);
		public List<Session> GetCourseSessions(string courseCode);
		public List<(IdentityUser user, string roleName)> GetCourseParticipants(string courseCode);
		public Task<Dictionary<IdentityUser, string>> GetCourseParticipantsAsync(string courseCode);
	}
}