﻿using System;
using Common.Data;
using Common.Protos.Session;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Net.Client;
using JetBrains.Annotations;

namespace FrontEnd.Services {
	public sealed class SessionService : IDisposable, ISessionService {
		private readonly SessionHandler.SessionHandlerClient _client;
		private readonly GrpcChannel _channel;

		public SessionService() {
			_channel = Startup.CreateStandardNewChannel;
			_client = new SessionHandler.SessionHandlerClient(_channel);
		}

		public SessionService(string host) {
			_channel = Startup.CreateNewChannel(host);
			_client = new SessionHandler.SessionHandlerClient(_channel);
		}

		[CanBeNull]
		public Session RequestSession(int sessionId) {
			SessionReply sessionReply = _client.RequestSession(new SessionRequestById() {SessionId = sessionId});
			return RequestConversions.CheckForSuccess(sessionReply.Request) ? sessionReply.Session.ToSession() : null;
		}

		public void RequestCreateSession(Session session, string courseCode) {
			RequestResult result = _client.RequestCreateSession(new SessionCreationRequest
				{Session = session.ToSessionReply(), OwningCourseCode = courseCode});
			if (RequestConversions.CheckForSuccess(result)) { }

			//TODO: Maybe display error
		}

		public void RequestDeleteSession(int sessionId) {
			RequestResult result = _client.RequestDeleteSession(new SessionRequestById() {SessionId = sessionId});
			if (RequestConversions.CheckForSuccess(result)) { }

			//TODO: Maybe display error
		}

		public void RequestUpdateSession(Session session) {
			RequestResult result = _client.RequestUpdateSession(session.ToSessionReply());
			if (RequestConversions.CheckForSuccess(result)) { }

			//TODO: Maybe display error
		}

		public void Dispose() {
			_channel?.Dispose();
		}
	}
}