﻿using Common.Data;
using Common.Protos.Session;
using Common.Protos.Utility;
using Common.Utility;
using JetBrains.Annotations;

namespace FrontEnd.Services {
	public interface ISessionService {
		[CanBeNull]
		public Session RequestSession(int sessionId);

		public void RequestCreateSession(Session session, string courseCode);

		public void RequestDeleteSession(int sessionId);

		public void RequestUpdateSession(Session session);
	}
}