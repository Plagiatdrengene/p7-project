﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Data;
using Common.Protos.Course;
using Common.Protos.PhysicalQuestion;
using Common.Protos.Session;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Net.Client;


namespace FrontEnd.Services {
	public sealed class QuestionService : IDisposable, IQuestionService {
		private readonly QuestionHandler.QuestionHandlerClient _client;
		private readonly GrpcChannel _channel;

		public QuestionService() {
			_channel = Startup.CreateStandardNewChannel;
			_client = new QuestionHandler.QuestionHandlerClient(_channel);
		}

		public QuestionService(string host) {
			_channel = Startup.CreateNewChannel(host);
			_client = new QuestionHandler.QuestionHandlerClient(_channel);
		}

		public RequestResult PhysicalQuestionCreationRequest(PhysicalQuestion physicalQuestion, int sessionId) {
			PhysicalQuestionCreatedReply reply =
				_client.CreatePhysicalQuestion(new PhysicalQuestionCreateRequest
					{Question = physicalQuestion.ToPhysicalQuestionProto(), SessionId = sessionId});
			if (RequestConversions.CheckForSuccess(reply.Result)) {
				return reply.Result;
			}

			Console.Error.WriteLine("Create Physicalquestion FAILED:\n" + string.Join("\n", reply.Result.Errors));
			return null;
		}


		public async Task<List<PhysicalQuestion>> GetAllPhysicalQuestionsForSession(int sessionId) {
			RepeatedPhysicalQuestion res = await _client.GetAllPhysicalQuestionsFromSessionAsync(new SessionRequestById
				{SessionId = sessionId});
			return RequestConversions.CheckForSuccess(res.Result)
				? res.Questions.Select(proto => proto.ToPhysicalQuestion()).ToList()
				: new List<PhysicalQuestion>();
		}

		public async Task<Question> GetQuestionById(int questionId) {
			var res = await _client.RequestQuestionByIdAsync(new QuestionIdProto {Id = questionId});
			if (RequestConversions.CheckForSuccess(res.Result)) {
				return res.ToQuestion();
			}
			
			await Console.Error.WriteLineAsync($"No Question with id {questionId} found...");
			return null;
		}

		public List<PhysicalQuestion> GetAllPhysicalQuestionsForCourse(string courseId) {
			RepeatedPhysicalQuestion res =
				_client.GetAllPhysicalQuestionsFromCourse(new CourseRequest {CourseCode = courseId});

			return RequestConversions.CheckForSuccess(res.Result)
				? res.Questions.Select(proto => proto.ToPhysicalQuestion()).ToList()
				: new List<PhysicalQuestion>();
		}

		public List<Question> GetSessionQuestionsWithoutAnswers(int sessionId) {
			var res = _client.RequestSessionQuestionsWithoutAnswers(new SessionRequestById {SessionId = sessionId});
			if (RequestConversions.CheckForSuccess(res.Result) && res.Questions is not null && res.Questions.Count != 0) {
				return res.Questions.Select(answers => answers.ToQuestion()).ToList();
			}
			return new List<Question>();
		}

		public async Task<List<Question>> GetSessionQuestionsWithoutAnswersAsync(int sessionId) {
			RepeatedQuestionNoAnswer res =
				 _client.RequestSessionQuestionsWithoutAnswers(new SessionRequestById
					{SessionId = sessionId});

			if (RequestConversions.CheckForSuccess(res.Result) && res.Questions is not null && res.Questions.Count != 0) {
				return res.Questions.Select(answers => answers.ToQuestion()).ToList();
			}

			return new List<Question>();
		}

		public RequestResult RequestCreateQuestion(Question question) {
			RequestResult result = _client.RequestCreateQuestion(question.ToCreateRequest());
			if (RequestConversions.CheckForSuccess(result)) {
				return result;
			}

			Console.Error.WriteLine("Create Question FAILED:\n" + string.Join("\n", result.Errors));
			return null;
		}

		public RequestResultWithId RequestCreateQuestionAnswer(QuestionAnswer questionAnswer) {
			RequestResultWithId result = _client.RequestCreateQuestionAnswer(questionAnswer.ToCreateQuestionAnswerCommentRequest());
			if (RequestConversions.CheckForSuccess(result.Result)) {
				return result;
			}
			Console.Error.WriteLine("Create QuestionAnswer FAILED:\n" + string.Join("\n", result.Result.Errors));
			return null;
		}
		
		public RequestResultWithId RequestCreateComment(Comment comment) {
			RequestResultWithId resultWithId = _client.RequestCreateComment(comment.ToCreateQuestionAnswerCommentRequest());
			if (RequestConversions.CheckForSuccess(resultWithId.Result)) {
				return resultWithId;
			}
			Console.Error.WriteLine("Create Comment FAILED:\n" + string.Join("\n", resultWithId.Result.Errors));
			return null;
		}

		public void Dispose() {
			_channel?.Dispose();
		}
	}
}