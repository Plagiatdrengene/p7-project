using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Data;
using Common.Protos.Course;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.Services {
	public sealed class CourseService : IDisposable, ICourseService {
		private readonly CourseHandler.CourseHandlerClient _client;
		private readonly GrpcChannel _channel;

		public CourseService() {
			_channel = Startup.CreateStandardNewChannel;
			_client = new CourseHandler.CourseHandlerClient(Startup.CreateStandardNewChannel);
		}

		public CourseService(string host) {
			_client = new CourseHandler.CourseHandlerClient(Startup.CreateNewChannel(host));
		}

		public Course RequestCourse(string courseCode) {
			CourseReply reply = _client.RequestCourse(new CourseRequest() {CourseCode = courseCode});
			return reply.ToCourse();
		}

		public TinyCourse RequestTinyCourse(string courseCode) {
			var reply = _client.RequestTinyCourse(new CourseRequest() {CourseCode = courseCode});
			return reply.ToTinyCourse();
		}

		public string CreateCourse(TinyCourse courseToCreate) {
			CourseCreatedReply reply = _client.CreateCourse(courseToCreate.ToTinyCourseReply());
			if (reply.Result.Succeeded)
				return reply.CourseCode;

			Console.Error.WriteLine("CreateCourse FAILED:\n" + string.Join("\n", reply.Result.Errors));
			return null;
		}

		public List<TinyCourse> GetAllCoursesThatUserIsPartOf(string userId) {
			var reply = _client.GetAllCoursesThatUserIsPartOf(new UserRequestById {UserId = userId});
			if (RequestConversions.CheckForSuccess(reply.Result))
				return reply.Courses.Select(tinyReply => tinyReply.ToTinyCourse()).ToList();
			return new List<TinyCourse>();
		}

		public bool IsUserPartOfCourse(string courseId, string userId) {
			var reply = _client.IsUserPartOfCourse(new UserCourseRequest() {CourseCode = courseId, UserId = userId});
			if (reply.Succeeded) {
				return true;
			}

			if (reply.Errors.Count != 0)
				Console.Error.WriteLine("CreateCourse FAILED:\n" + string.Join("\n", reply.Errors));
			return false;
		}

		public bool RemoveUserFromCourse(string courseId, string userId) {
			var reply = _client.RemoveUserFromCourse(new UserCourseRequest {CourseCode = courseId, UserId = userId});

			if (!reply.Succeeded) {
				Console.Error.WriteLine("Remove User From Course FAILED:\n" + string.Join("\n", reply.Errors));
			}

			return reply.Succeeded;
		}

		public RequestResult JoinCourse(string courseCode, string userId) {
			return _client.JoinCourse(new UserCourseRequest {CourseCode = courseCode, UserId = userId});
		}

		public List<Session> GetCourseSessions(string courseCode) {
			return _client.GetCourseSessions(new CourseRequest {CourseCode = courseCode}).Sessions
				.Select(reply => reply.ToSession()).ToList();
		}

		public List<(IdentityUser user, string roleName)> GetCourseParticipants(string courseCode) {
			RepeatedUserCourseRole reply = _client.GetAllUsersPartOfCourse(new CourseRequest {CourseCode = courseCode});
			if (RequestConversions.CheckForSuccess(reply.Result))
				return reply.Users.Select(role => (role.User.ToIdentityUser(), role.RoleName)).ToList();
			return new List<(IdentityUser user, string Name)>();
		}

		public async Task<Dictionary<IdentityUser, string>> GetCourseParticipantsAsync(string courseCode) {
			RepeatedUserCourseRole reply =
				await _client.GetAllUsersPartOfCourseAsync(new CourseRequest {CourseCode = courseCode});
			if (RequestConversions.CheckForSuccess(reply.Result))
				return reply.Users.ToDictionary(role => (role.User.ToIdentityUser()), role => role.RoleName);
			return new Dictionary<IdentityUser, string>();
		}


		public void Dispose() {
			_channel.Dispose();
		}
	}
}