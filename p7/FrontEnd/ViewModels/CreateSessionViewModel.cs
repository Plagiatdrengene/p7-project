﻿using System;
using System.ComponentModel.DataAnnotations;
using Common.Data;

namespace FrontEnd.ViewModels {
	public class CreateSessionViewModel : BaseViewModel {
		[Required] public string Name { get; set; }
		[Required] public DateTime StartDate { get; set; }

		[Required] public DateTime EndDate { get; set; }

		public void Reset() {
			Name = "";
			StartDate = default;
			EndDate = default;
		}

	}
}