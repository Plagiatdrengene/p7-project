using System;
using System.Collections.Generic;
using System.Linq;
using Common.Data;
using Common.PubSub.Data;
using FrontEnd.PubSub;
using FrontEnd.Services;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.ViewModels {
	public class QuestionViewModel : BaseViewModel, IDisposable {
		private Question _question;
		private bool _isInitialized;
		private readonly int _questionId;
		private readonly QuestionAnswerSubscription _answerSubscription;
		private readonly CommentSubscription _commentSubscription;

		public Question Question {
			get {
				if (_isInitialized) return _question;
				Refresh();
				_isInitialized = true;

				return _question;
			}
			set => SetValue(ref _question, value);
		}

		public QuestionViewModel(int questionId, QuestionAnswerSubscription answerSubscription,
			CommentSubscription commentSubscription) {
			_questionId = questionId;
			_answerSubscription = answerSubscription;
			_answerSubscription.PostableEvent.Subscribe(OnQuestionAnswerUpdate);
			_commentSubscription = commentSubscription;
			_commentSubscription.PostableEvent.Subscribe(OnCommentUpdate);
		}

		public string GetTitle() {
			return Question?.Title ?? "";
		}

		public bool GetIsSolved() {
			return Question?.IsSolved ?? false;
		}

		public string GetContent() {
			return Question?.Content ?? "";
		}

		public IdentityUser GetPoster() {
			return Question?.Poster;
		}

		public DateTime GetPostDate() {
			return Question?.PostDate ?? DateTime.MinValue;
		}

		public void OnCommentUpdate(PostableUpdate postableUpdate) {
			switch (postableUpdate.UpdateType) {
				case UpdateType.Created:
				case UpdateType.Deleted:
				case UpdateType.Updated:
					Refresh();
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(UpdateType), postableUpdate.UpdateType, null);
			}

			OnPropertyChanged();
		}

		public void OnQuestionAnswerUpdate(PostableUpdate postableUpdate) {
			if (_questionId != postableUpdate.PostableId) return;
			switch (postableUpdate.UpdateType) {
				case UpdateType.Created:
				case UpdateType.Deleted:
				case UpdateType.Updated:
					Refresh();
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(UpdateType), postableUpdate.UpdateType, null);
			}

			OnPropertyChanged();
		}

		public async void Refresh() {
			IsBusy = true;
			using var qService = new QuestionService();
			Question = await qService.GetQuestionById(_questionId);
			IsBusy = false;
		}

		public IEnumerable<QuestionAnswer> GetQuestionAnswers() {
			return Question.QuestionAnswers
				.OrderByDescending(answer => answer.IsMarkedCorrect)
				.ThenBy(answer => answer.PostDate)
				.ToList();
		}

		public IEnumerable<Comment> GetComments(QuestionAnswer answer) {
			return answer.Comments.OrderBy(comment => comment.PostDate).ToList();
		}

		public void Dispose() {
			_answerSubscription.PostableEvent.UnSubscribe(OnQuestionAnswerUpdate);
			_commentSubscription.PostableEvent.UnSubscribe(OnCommentUpdate);
		}
	}
}