using System.Collections.Generic;
using System.Linq;
using FrontEnd.Services;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace FrontEnd.ViewModels {
	public class IdentityUserViewModel : BaseViewModel {
		private List<IdentityUser> _users;

		public IdentityUserViewModel(UserManager<IdentityUser> userManager) {
			_users = userManager.Users.ToList();
		}

		[NotNull]
		public List<IdentityUser> Users {
			get => _users;
			private set => SetValue(ref _users, value);
		}
	}
}