using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Data;
using Common.PubSub.Data;
using FrontEnd.Pages.CoursePages.Components;
using FrontEnd.PubSub;
using FrontEnd.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Course = Common.Data.Course;

namespace FrontEnd.ViewModels {
	public class TinyCourseViewModel : BaseViewModel, IDisposable {
		private TinyCourse _course = new Course();
		private List<Session> _sessions = new();
		private readonly QuestionSubscription _questionSubscription;
		private List<Question> _questions = new();
		private bool _isQuestionsInitialized;
		private bool _isSessionsInitialized;

		public virtual List<Question> Questions {
			get {
				if (_isQuestionsInitialized) return _questions;
				IsBusy = true;
				_isQuestionsInitialized = true;
				UpdateActiveQuestions();
				IsBusy = false;
				return _questions;
			}
			private set => SetValue(ref _questions, value);
		}

		public virtual Session SelectedSession { get; set; }

		public Session ActiveSession => Sessions.FirstOrDefault(session => session.IsActive());


		public TinyCourseViewModel(TinyCourse course) {
			Course = course;
		}

		public TinyCourseViewModel(string courseCode, QuestionSubscription questionSubscription,
			NavigationManager navigationManager) {
			_questionSubscription = questionSubscription;
			_questionSubscription.PostableEvent.Subscribe(OnQuestionUpdate);
			using var courseService = new CourseService();
			Course = courseService.RequestTinyCourse(courseCode);
			if (Course == null) {
				navigationManager.NavigateTo("/CourseNotFound");
			}

			if (Sessions.Count > 0 && ActiveSession is null) {
				SelectedSession = SessionComponent.DefaultSession;
			}
			else if (Sessions.Count == 0) {
				SelectedSession = SessionComponent.DefaultSession;
			}
			else {
				SelectedSession = ActiveSession;
			}
		}

		public List<Question> GetSessionQuestionsWithoutAnswers(Session session) {
			if (session is null) {
				return new List<Question>();
			}

			using var questionService = new QuestionService();
			return questionService.GetSessionQuestionsWithoutAnswers(session.Id);
		}

		public async Task<List<Question>> GetAllCourseQuestions() {
			using var questionService = new QuestionService();
			List<Session> sessions = Sessions;
			List<Task<List<Question>>> tasks = new List<Task<List<Question>>>();
			foreach (Session session in sessions) {
				tasks.Add(questionService.GetSessionQuestionsWithoutAnswersAsync(session.Id));
			}


			List<Question> res = new List<Question>();

			await Task.WhenAll(tasks);
			tasks.ForEach(task => res.AddRange(task.Result));
			return res;
		}


		public virtual bool RemoveUserFromCourse(IdentityUser userToRemove) {
			IsBusy = true;
			using var courseService = new CourseService();
			bool res = courseService.RemoveUserFromCourse(_course.CourseCode, userToRemove.Id);
			IsBusy = false;
			return res;
		}

		public TinyCourseViewModel() { }

		public virtual List<Session> Sessions {
			get {
				if (_isSessionsInitialized) return _sessions;
				IsBusy = true;
				_isSessionsInitialized = true;
				using var courseService = new CourseService();
				_sessions.AddRange(courseService.GetCourseSessions(_course.CourseCode));
				IsBusy = false;
				return _sessions;
			}
			private set => SetValue(ref _sessions, value);
		}

		public void ClearSessions() {
			_isSessionsInitialized = false;
			_sessions.Clear();
		}

		public virtual TinyCourse Course {
			get => _course;
			private set => SetValue(ref _course, value);
		}

		public void UpdateActiveQuestions() {
			if (SelectedSession is not null &&
			    SelectedSession.Name == SessionComponent.DefaultSession.Name) {
				_questions = GetAllCourseQuestions().Result;
			}
			else {
				_questions = GetSessionQuestionsWithoutAnswers(SelectedSession);
			}

			_questions.Sort((q1, q2) => DateTime.Compare(q2.PostDate, q1.PostDate));
			OnPropertyChanged(nameof(Questions));
		}

		public async void OnQuestionUpdate(PostableUpdate postableUpdate) {
			if (!postableUpdate.CourseCode.Equals(Course.CourseCode, StringComparison.OrdinalIgnoreCase)) return;
			switch (postableUpdate.UpdateType) {
				case UpdateType.Created:
					using (var qService = new QuestionService()) {
						_questions.Add(await qService.GetQuestionById(postableUpdate.PostableId));
					}

					break;
				case UpdateType.Deleted:
					break;
				case UpdateType.Updated:
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(UpdateType), postableUpdate.UpdateType, null);
			}

			OnPropertyChanged();
		}

		public void Dispose() {
			_questionSubscription?.PostableEvent.UnSubscribe(OnQuestionUpdate);
		}
	}
}