using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace FrontEnd.ViewModels {
	public abstract class BaseViewModel : INotifyPropertyChanged {
		private bool _isBusy;

		public bool IsBusy {
			get => _isBusy;
			set => SetValue(ref _isBusy, value);
		}

		public event PropertyChangedEventHandler PropertyChanged = null!;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null!) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		protected void SetValue<T>(ref T backingFiled, T value, [CallerMemberName] string propertyName = null!) {
			if (EqualityComparer<T>.Default.Equals(backingFiled, value)) return;
			backingFiled = value;
			OnPropertyChanged(propertyName);
		}
	}
}