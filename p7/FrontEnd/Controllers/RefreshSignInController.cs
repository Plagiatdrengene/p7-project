﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace FrontEnd.Controllers {
	public class RefreshSignInController : Controller {
		[Route("api/[controller]")]
		[HttpPost("[action]")]
		[Authorize]
		public async Task<IActionResult> RefreshSignInPage() {
			await RefreshLoginAsync(HttpContext);
			return Redirect("~/");
		}

		// GET
		[Route("api/[controller]/{backUrl}")]
		[HttpPost("[action]")]
		[Authorize]
		public async Task<IActionResult> RefreshSignInPage(string backUrl) {
			await RefreshLoginAsync(HttpContext);
			return Redirect(Request.Path.Value.ToLower().Replace("api/refreshsignin/", "").Replace("-", "/"));
		}

		public static async Task RefreshLoginAsync(HttpContext context) {
			if (context.User == null)
				return;

			var userManager = context.RequestServices
				.GetRequiredService<UserManager<IdentityUser>>();
			var signInManager = context.RequestServices
				.GetRequiredService<SignInManager<IdentityUser>>();

			IdentityUser user = await userManager.GetUserAsync(context.User);

			if (signInManager.IsSignedIn(context.User)) {
				await signInManager.RefreshSignInAsync(user);
			}
		}
	}
}