using System;
using System.Linq;
using BackEnd.Database;
using BackEnd.Services;
using Common.Protos.Roles;
using Common.Protos.User;
using Common.Protos.Utility;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace BackEnd.Tests {
	public class RoleServiceTests {
		private DataForTest _dataForTest;
		private RoleService _roleService;

		[SetUp]
		public void SetUp() {
			_dataForTest = new DataForTest();

			var userManager = new UserManager<IdentityUserCustom>(new UserStore<IdentityUserCustom>(_dataForTest.Db),
				null, null, null, null, new UpperInvariantLookupNormalizer(), null, null,
				new NullLogger<UserManager<IdentityUserCustom>>());
			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_dataForTest.Db),
				null, new UpperInvariantLookupNormalizer(), null, new NullLogger<RoleManager<IdentityRole>>());

			_roleService = new RoleService(userManager, _dataForTest.Db, roleManager);
		}

		[Test]
		public void RequestIsInRole_Succeed() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User1.Id, NormalizedRoleName = "STAFF"};

			// Act
			UserIsInRoleReply result = _roleService.RequestIsInRole(userRoleRequestByName, null).Result;

			// Assert
			Assert.True(result.RequestResult.Succeeded);
		}

		[Test]
		public void RequestIsInRole_IsInRole() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User1.Id, NormalizedRoleName = "STAFF"};

			// Act
			UserIsInRoleReply result = _roleService.RequestIsInRole(userRoleRequestByName, null).Result;

			// Assert
			Assert.True(result.IsInRole);
		}

		[Test]
		public void RequestIsInRole_NotInRole() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User2.Id, NormalizedRoleName = "STAFF"};

			// Act
			UserIsInRoleReply result = _roleService.RequestIsInRole(userRoleRequestByName, null).Result;

			// Assert
			Assert.False(result.IsInRole);
		}

		[Test]
		public void RequestIsInRole_UserMustExist() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName() {UserId = "0", NormalizedRoleName = "STAFF"};

			// Act
			UserIsInRoleReply result = _roleService.RequestIsInRole(userRoleRequestByName, null).Result;

			// Assert
			Assert.False(result.RequestResult.Succeeded);
		}

		[Test]
		public void RequestUserRole_Succeed() {
			// Arrange
			var roleRequestByName = new UserRoleRequestById()
				{RoleId = _dataForTest.StaffRole.Id, UserId = _dataForTest.User1.Id};

			// Act
			UserRole result = _roleService.RequestUserRole(roleRequestByName, null).Result;

			// Assert
			Assert.True(result.Result.Succeeded);
		}

		[Test]
		public void RequestUserRole_ReturnsRightRole() {
			// Arrange
			var roleRequestByName = new UserRoleRequestById()
				{RoleId = _dataForTest.StaffRole.Id, UserId = _dataForTest.User1.Id};

			// Act
			UserRole result = _roleService.RequestUserRole(roleRequestByName, null).Result;

			// Assert
			Assert.True(result.Result.Succeeded);
			Assert.AreEqual("StaffRole", result.RoleId);
			Assert.AreEqual("1", result.UserId);
		}

		[Test]
		public void RequestUserRole_RoleMustExist() {
			// Arrange
			var roleRequestByName = new UserRoleRequestById() {RoleId = "INVALIDNAME", UserId = _dataForTest.User1.Id};

			// Act
			UserRole result = _roleService.RequestUserRole(roleRequestByName, null).Result;

			// Assert
			Assert.False(result.Result.Succeeded);
			Assert.AreEqual("Failed to find user or role.", result.Result.Errors[0]);
		}

		[Test]
		public void RequestUsersInRole_ReturnsAll() {
			// Arrange
			var roleRequestByName = new RoleRequestByName() {NormalizedName = _dataForTest.StaffRole.NormalizedName};

			// Act
			UserListReply result = _roleService.RequestUsersInRole(roleRequestByName, null).Result;

			// Assert
			Assert.AreEqual(1, result.Users.Count);
		}

		[Test]
		public void RequestUsersInRole_ReturnsRightUser() {
			// Arrange
			var roleRequestByName = new RoleRequestByName() {NormalizedName = _dataForTest.StaffRole.NormalizedName};

			// Act
			UserListReply result = _roleService.RequestUsersInRole(roleRequestByName, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.User1.Id, result.Users[0].UserId);
		}

		[Test]
		public void RequestUsersInRole_ReturnsNoneIfNonExistingRole() {
			// Arrange
			var roleRequestByName = new RoleRequestByName() {NormalizedName = "INVALIDROLE"};

			// Act
			UserListReply result = _roleService.RequestUsersInRole(roleRequestByName, null).Result;

			// Assert
			Assert.IsEmpty(result.Users);
		}

		[Test]
		public void RequestAddToRole_Succeed() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User2.Id, NormalizedRoleName = _dataForTest.StaffRole.NormalizedName};

			// Act
			RequestResult result = _roleService.RequestAddToRole(userRoleRequestByName, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}

		[Test]
		public void RequestAddToRole_UserMustExist() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = "0", NormalizedRoleName = _dataForTest.StaffRole.NormalizedName};

			// Act
			RequestResult result = _roleService.RequestAddToRole(userRoleRequestByName, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}


		[Test]
		public void RequestAddToRole_RoleIsAdded() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User2.Id, NormalizedRoleName = _dataForTest.StaffRole.NormalizedName};
			int amountOfUserRoles = _dataForTest.Db.UserRoles.ToList().Count;

			// Act
			RequestResult result = _roleService.RequestAddToRole(userRoleRequestByName, null).Result;
			int amountOfUserRolesAfter = _dataForTest.Db.UserRoles.ToList().Count;

			// Assert
			Assert.AreEqual(amountOfUserRoles + 1, amountOfUserRolesAfter);
		}

		[Test]
		public void RequestAddToRole_RoleMustExist() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User2.Id, NormalizedRoleName = "INVALIDROLE"};
			int amountOfUserRoles = _dataForTest.Db.UserRoles.ToList().Count;

			// Act
			RequestResult result = _roleService.RequestAddToRole(userRoleRequestByName, null).Result;
			int amountOfUserRolesAfter = _dataForTest.Db.UserRoles.ToList().Count;

			// Assert
			Assert.False(result.Succeeded);
			Assert.AreEqual(amountOfUserRoles, amountOfUserRolesAfter);
		}

		[Test]
		public void RequestRemoveFromRole_Succeed() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User1.Id, NormalizedRoleName = _dataForTest.StaffRole.NormalizedName};

			// Act
			RequestResult result = _roleService.RequestRemoveFromRole(userRoleRequestByName, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}

		[Test]
		public void RequestRemoveFromRole_RemovesRole() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User1.Id, NormalizedRoleName = _dataForTest.StaffRole.NormalizedName};
			int amountOfUserRoles = _dataForTest.Db.UserRoles.ToList().Count;

			// Act
			RequestResult result = _roleService.RequestRemoveFromRole(userRoleRequestByName, null).Result;
			int amountOfUserRolesAfter = _dataForTest.Db.UserRoles.ToList().Count;

			// Assert
			Assert.AreEqual(amountOfUserRoles - 1, amountOfUserRolesAfter);
		}

		[Test]
		public void RequestRemoveFromRole_UserMustExist() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = "0", NormalizedRoleName = _dataForTest.StaffRole.NormalizedName};
			int amountOfUserRoles = _dataForTest.Db.UserRoles.ToList().Count;

			// Act
			RequestResult result = _roleService.RequestRemoveFromRole(userRoleRequestByName, null).Result;
			int amountOfUserRolesAfter = _dataForTest.Db.UserRoles.ToList().Count;

			// Assert
			Assert.False(result.Succeeded);
			Assert.AreEqual(amountOfUserRoles, amountOfUserRolesAfter);
		}

		[Test]
		public void RequestRemoveFromRole_RoleMustExist() {
			// Arrange
			var userRoleRequestByName = new UserRoleRequestByName()
				{UserId = _dataForTest.User1.Id, NormalizedRoleName = "INVALIDROLE"};
			int amountOfUserRoles = _dataForTest.Db.UserRoles.ToList().Count;

			// Act
			RequestResult result = _roleService.RequestRemoveFromRole(userRoleRequestByName, null).Result;
			int amountOfUserRolesAfter = _dataForTest.Db.UserRoles.ToList().Count;

			// Assert
			Assert.False(result.Succeeded);
			Assert.AreEqual(amountOfUserRoles, amountOfUserRolesAfter);
		}

		[Test]
		public void RequestGetRoles_GetsAll() {
			// Arrange
			var userRequestById = new UserRequestById() {UserId = _dataForTest.User1.Id};

			// Act
			RoleNameList result = _roleService.RequestGetRoles(userRequestById, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.Db.UserRoles.ToList().FindAll(r => r.UserId == _dataForTest.User1.Id).Count,
				result.Roles.Count);
		}

		[Test]
		public void RequestGetRoles_ReturnsEmptyListIfUserDoesNotExist() {
			// Arrange
			var userRequestById = new UserRequestById() {UserId = "0"};

			// Act
			RoleNameList result = _roleService.RequestGetRoles(userRequestById, null).Result;

			// Assert
			Assert.IsEmpty(result.Roles);
		}


		[Test]
		public void RequestCreateRole_Succeed() {
			// Arrange
			var roleReply = new RoleReply() {Id = "testRoleId", Name = "testRoleName", NormalizedName = "TESTROLENAME"};

			// Act
			RequestResult result = _roleService.RequestCreateRole(roleReply, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}

		[Test]
		public void RequestCreateRole_CreatesTheRole() {
			// Arrange
			var roleReply = new RoleReply() {Id = "testRoleId", Name = "testRoleName", NormalizedName = "TESTROLENAME"};
			int amountOfRoles = _dataForTest.Db.Roles.ToList().Count;

			// Act
			RequestResult result = _roleService.RequestCreateRole(roleReply, null).Result;
			int amountOfRolesAfter = _dataForTest.Db.Roles.ToList().Count;

			// Assert
			Assert.AreEqual(amountOfRoles + 1, amountOfRolesAfter);
		}

		[Test]
		public void RequestCreateRole_RoleIdMustBeUnique() {
			// Arrange
			var roleReply = new RoleReply()
				{Id = _dataForTest.StaffRole.Id, Name = "testRoleName", NormalizedName = "TESTROLENAME"};

			// Act
			RequestResult result = _roleService.RequestCreateRole(roleReply, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void RequestUpdateRole_Succeed() {
			// Arrange
			var roleReply = new RoleReply()
				{Id = _dataForTest.StaffRole.Id, Name = "testRoleName", NormalizedName = "TESTROLENAME"};

			// Act
			RequestResult result = _roleService.RequestUpdateRole(roleReply, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}

		[Test]
		public void RequestUpdateRole_Updates() {
			// Arrange
			var newname = "newName";
			var roleReply = new RoleReply()
				{Id = _dataForTest.StaffRole.Id, Name = newname, ConcurrencyStamp = "qwe", NormalizedName = "NEWNAME"};

			// Act
			RequestResult result = _roleService.RequestUpdateRole(roleReply, null).Result;

			// Assert
			Assert.AreEqual(newname, _dataForTest.Db.Roles.Find(_dataForTest.StaffRole.Id).Name);
		}

		[Test]
		public void RequestUpdateRole_RoleIdMustExist() {
			// Arrange
			var roleReply = new RoleReply()
				{Id = "invalidId", Name = "newName", ConcurrencyStamp = "qwe", NormalizedName = "NEWNAME"};

			// Act
			RequestResult result = _roleService.RequestUpdateRole(roleReply, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void RequestDeleteRole_Succeed() {
			// Arrange
			var roleRequestById = new RoleRequestById() {RoleId = _dataForTest.StaffRole.Id};

			// Act
			RequestResult result = _roleService.RequestDeleteRole(roleRequestById, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}

		[Test]
		public void RequestDeleteRole_DeletesTheRole() {
			// Arrange
			var roleRequestById = new RoleRequestById() {RoleId = _dataForTest.StaffRole.Id};
			int amountOfRoles = _dataForTest.Db.Roles.ToList().Count;

			// Act
			RequestResult result = _roleService.RequestDeleteRole(roleRequestById, null).Result;
			int amountOfRolesafter = _dataForTest.Db.Roles.ToList().Count;

			// Assert
			Assert.AreEqual(amountOfRoles - 1, amountOfRolesafter);
		}

		[Test]
		public void RequestDeleteRole_RoleMustExist() {
			// Arrange
			var roleRequestById = new RoleRequestById() {RoleId = "invalidRoleId"};

			// Act
			RequestResult result = _roleService.RequestDeleteRole(roleRequestById, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void RequestRoleById_ReturnsRole() {
			// Arrange
			var roleRequestById = new RoleRequestById() {RoleId = _dataForTest.StaffRole.Id};

			// Act
			RoleReply result = _roleService.RequestRoleById(roleRequestById, null).Result;

			// Assert
			Assert.True(result.RequestResult.Succeeded);
			Assert.AreEqual(typeof(RoleReply), result.GetType());
			Assert.IsNotNull(result);
		}

		[Test]
		public void RequestRoleById_ReturnsRightRole() {
			// Arrange
			var roleRequestById = new RoleRequestById() {RoleId = _dataForTest.StaffRole.Id};

			// Act
			RoleReply result = _roleService.RequestRoleById(roleRequestById, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.StaffRole.Id, result.Id);
			Assert.AreEqual(_dataForTest.StaffRole.Name, result.Name);
			Assert.AreEqual(_dataForTest.StaffRole.NormalizedName, result.NormalizedName);
			Assert.AreEqual(_dataForTest.StaffRole.ConcurrencyStamp, result.ConcurrencyStamp);
		}

		[Test]
		public void RequestRoleById_RoleMustExist() {
			// Arrange
			var roleRequestById = new RoleRequestById() {RoleId = "invalidRoleId"};

			// Act
			RoleReply result = _roleService.RequestRoleById(roleRequestById, null).Result;

			// Assert
			Assert.False(result.RequestResult.Succeeded);
		}

		[Test]
		public void RequestRoleByName_ReturnsRole() {
			// Arrange
			var roleRequestByName = new RoleRequestByName() {NormalizedName = _dataForTest.StaffRole.NormalizedName};

			// Act
			RoleReply result = _roleService.RequestRoleByName(roleRequestByName, null).Result;

			// Assert
			Assert.True(result.RequestResult.Succeeded);
			Assert.AreEqual(typeof(RoleReply), result.GetType());
			Assert.IsNotNull(result);
		}

		[Test]
		public void RequestRoleByName_ReturnsRightRole() {
			// Arrange
			var roleRequestByName = new RoleRequestByName() {NormalizedName = _dataForTest.StaffRole.NormalizedName};

			// Act
			RoleReply result = _roleService.RequestRoleByName(roleRequestByName, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.StaffRole.Id, result.Id);
			Assert.AreEqual(_dataForTest.StaffRole.Name, result.Name);
			Assert.AreEqual(_dataForTest.StaffRole.NormalizedName, result.NormalizedName);
			Assert.AreEqual(_dataForTest.StaffRole.ConcurrencyStamp, result.ConcurrencyStamp);
		}

		[Test]
		public void RequestRoleByName_RoleMustExist() {
			// Arrange
			var roleRequestByName = new RoleRequestByName() {NormalizedName = "INVALIDNAME"};

			// Act
			RoleReply result = _roleService.RequestRoleByName(roleRequestByName, null).Result;

			// Assert
			Assert.False(result.RequestResult.Succeeded);
		}


		[Test]
		public void RequestAllRoles_ReturnsRoles() {
			// Arrange

			// Act
			RoleList result = _roleService.RequestAllRoles(null, null).Result;

			// Assert
			Assert.IsNotEmpty(result.Roles);
			Assert.AreEqual(typeof(RoleReply), result.Roles[0].GetType());
		}

		[Test]
		public void RequestAllRoles_GetsAllRoles() {
			// Arrange
			int amountOfRoles = _dataForTest.Db.Roles.ToList().Count;

			// Act
			RoleList result = _roleService.RequestAllRoles(null, null).Result;

			// Assert
			Assert.AreEqual(amountOfRoles, result.Roles.Count);
		}

		[Test]
		public void RequestAllRoles_GetsRightRoles() {
			// Arrange

			// Act
			RoleList result = _roleService.RequestAllRoles(null, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.StaffRole.Id, result.Roles[0].Id);
			Assert.AreEqual(_dataForTest.NonStaffRole.Id, result.Roles[1].Id);
		}
	}
}