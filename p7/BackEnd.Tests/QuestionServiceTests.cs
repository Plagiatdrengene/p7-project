﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd.Database;
using BackEnd.PubSub;
using BackEnd.Services;
using Common.Data;
using Common.Protos.Course;
using Common.Protos.PhysicalQuestion;
using Common.Protos.Session;
using Common.Protos.Utility;
using Common.Utility;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Identity;
using NUnit.Framework;


namespace BackEnd.Tests {
	public class QuestionServiceTests {
		private DataForTest _dataForTest;
		private QuestionService _questionService;

		[SetUp]
		public void SetUp() {
			_dataForTest?.Dispose();
			_dataForTest = new DataForTest();
			_questionService = new QuestionService(_dataForTest.Db, new PhysicalQuestionPublisher(), new QuestionPublisher(), new CommentPublisher(), new QuestionAnswerPublisher());
		}

		[Test]
		public void CreatePhysicalQuestion_CanCreate() {
			// Arrange
			var physicalQuestionProto = new PhysicalQuestionProto()
				{Title = "testpqTitle", Location = "testpqLocation", User = _dataForTest.User2.ToUserReplySafe(), 
					Content = "testpqContent", PostDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())};
			var physicalQuestionCreateRequest = new PhysicalQuestionCreateRequest(){Question = physicalQuestionProto, SessionId = _dataForTest.Session1.Id};
			int amountOfPhysicalQuestions = _dataForTest.Db.PhysicalQuestions.ToList()
                .FindAll(q => q.OwningSession.Id == _dataForTest.Session1.Id).Count;

			// Act
			PhysicalQuestionCreatedReply result =  _questionService.CreatePhysicalQuestion(physicalQuestionCreateRequest, null).Result;
			int amountOfPhysicalQuestionsAfter = _dataForTest.Db.PhysicalQuestions.ToList()
                .FindAll(q => q.OwningSession.Id == _dataForTest.Session1.Id).Count;

			// Assert
			Assert.True(result.Result.Succeeded);
			Assert.AreEqual(amountOfPhysicalQuestions+1, amountOfPhysicalQuestionsAfter);
		}

		[Test]
		public void CreatePhysicalQuestion_UserMustExist() {
			// Arrange
			var user = new IdentityUserCustom(){Id = "0", Email = "0@0.com", UserName = "userName0", LockoutEnabled = false, EmailConfirmed = true};
			var physicalQuestionProto = new PhysicalQuestionProto()
			{Title = "testpqTitle", Location = "testpqLocation", User = user.ToUserReplySafe(), 
				Content = "testpqContent", PostDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())};
			var physicalQuestionCreateRequest = new PhysicalQuestionCreateRequest(){Question = physicalQuestionProto, SessionId = _dataForTest.Session1.Id};
			int amountOfPhysicalQuestions = _dataForTest.Db.PhysicalQuestions.ToList()
				.FindAll(q => q.OwningSession.Id == _dataForTest.Session1.Id).Count;
			
			// Act
			PhysicalQuestionCreatedReply result = _questionService.CreatePhysicalQuestion(physicalQuestionCreateRequest, null).Result;
			int amountOfPhysicalQuestionsAfter = _dataForTest.Db.PhysicalQuestions.ToList()
				.FindAll(q => q.OwningSession.Id == _dataForTest.Session1.Id).Count;
			
			// Assert
			Assert.False(result.Result.Succeeded);
			Assert.AreEqual(amountOfPhysicalQuestions, amountOfPhysicalQuestionsAfter);
			
		}

		[Test]
		public void CreatePhysicalQuestion_SessionMustExist() {
			// Arrange
			var physicalQuestionProto = new PhysicalQuestionProto()
			{Title = "testpqTitle", Location = "testpqLocation", User = _dataForTest.User2.ToUserReplySafe(), 
				Content = "testpqContent", PostDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime())};
			var physicalQuestionCreateRequest = new PhysicalQuestionCreateRequest(){Question = physicalQuestionProto, SessionId = 0};
			int amountOfPhysicalQuestions = _dataForTest.Db.PhysicalQuestions.ToList().Count;

			// Act
			PhysicalQuestionCreatedReply result = _questionService.CreatePhysicalQuestion(physicalQuestionCreateRequest, null).Result;
			int amountOfPhysicalQuestionsAfter = _dataForTest.Db.PhysicalQuestions.ToList().Count;
			
			// Assert
			Assert.False(result.Result.Succeeded);
			Assert.AreEqual(amountOfPhysicalQuestions, amountOfPhysicalQuestionsAfter);
		}

		[Test]
		public void GetAllPhysicalQuestionsFromSession_ReturnsAll() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = _dataForTest.Session1.Id};
			int amountOfPhysicalQuestions = _dataForTest.Db.PhysicalQuestions.ToList()
				.FindAll(q => q.OwningSession.Id == _dataForTest.Session1.Id && q.GetType() == typeof(PhysicalQuestion)).Count;

			// Act
			RepeatedPhysicalQuestion result = _questionService.GetAllPhysicalQuestionsFromSession(sessionRequestById, null).Result;

			// Assert
			Assert.AreEqual(amountOfPhysicalQuestions, result.Questions.Count);
		}

		[Test]
		public void GetAllPhysicalQuestionsFromSession_ReturnsRightQuestions() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = _dataForTest.Session1.Id};

			// Act
			RepeatedPhysicalQuestion result = _questionService.GetAllPhysicalQuestionsFromSession(sessionRequestById, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.PhysicalQuestion1.Title, result.Questions[0].Title);
			Assert.AreEqual(_dataForTest.PhysicalQuestion1.Location, result.Questions[0].Location);
			Assert.AreEqual(_dataForTest.PhysicalQuestion1.Content, result.Questions[0].Content);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.PhysicalQuestion1.PostDate.ToUniversalTime()),  result.Questions[0].PostDate);
			Assert.AreEqual(_dataForTest.PhysicalQuestion1.Poster.Id,  result.Questions[0].User.UserId);
			
			Assert.AreEqual(_dataForTest.PhysicalQuestion2.Title, result.Questions[1].Title);
			Assert.AreEqual(_dataForTest.PhysicalQuestion2.Location, result.Questions[1].Location);
			Assert.AreEqual(_dataForTest.PhysicalQuestion2.Content, result.Questions[1].Content);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.PhysicalQuestion2.PostDate.ToUniversalTime()),  result.Questions[1].PostDate);
			Assert.AreEqual(_dataForTest.PhysicalQuestion2.Poster.Id,  result.Questions[1].User.UserId);
		}

		[Test]
		public void GetAllPhysicalQuestionsFromSession_SessionMustExist() {
			// Arrange
			var sessionRequestById = new SessionRequestById() {SessionId = 0};

			// Act
			RepeatedPhysicalQuestion result = _questionService.GetAllPhysicalQuestionsFromSession(sessionRequestById, null).Result;

			// Assert
			Assert.False(result.Result.Succeeded);
		}
		
		[Test]
		public void GetAllPhysicalQuestionsFromCourse_ReturnsAll() {
			// Arrange
			var courseRequest = new CourseRequest(){CourseCode = _dataForTest.Course1.CourseCode};
			List<Session> sessions = _dataForTest.Course1.Sessions;
			int amountOfPhysicalQuestions = sessions.Sum(session => _dataForTest.Db.PhysicalQuestions.ToList()
				.FindAll(q => q.OwningSession.Id == session.Id)
				.Count);

			// Act
			RepeatedPhysicalQuestion result = _questionService.GetAllPhysicalQuestionsFromCourse(courseRequest,null).Result;

			// Assert
			Assert.AreEqual(amountOfPhysicalQuestions, result.Questions.Count);
		}

		[Test]
		public void GetAllPhysicalQuestionsFromCourse_ReturnsRightQuestions() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			RepeatedPhysicalQuestion result = _questionService.GetAllPhysicalQuestionsFromCourse(courseRequest,null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.PhysicalQuestion1.Title, result.Questions[0].Title);
			Assert.AreEqual(_dataForTest.PhysicalQuestion1.Location, result.Questions[0].Location);
			Assert.AreEqual(_dataForTest.PhysicalQuestion1.Content, result.Questions[0].Content);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.PhysicalQuestion1.PostDate.ToUniversalTime()),  result.Questions[0].PostDate);
			Assert.AreEqual(_dataForTest.PhysicalQuestion1.Poster.Id,  result.Questions[0].User.UserId);
			
			Assert.AreEqual(_dataForTest.PhysicalQuestion2.Title, result.Questions[1].Title);
			Assert.AreEqual(_dataForTest.PhysicalQuestion2.Location, result.Questions[1].Location);
			Assert.AreEqual(_dataForTest.PhysicalQuestion2.Content, result.Questions[1].Content);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.PhysicalQuestion2.PostDate.ToUniversalTime()),  result.Questions[1].PostDate);
			Assert.AreEqual(_dataForTest.PhysicalQuestion2.Poster.Id,  result.Questions[1].User.UserId);
		}

		[Test]
		public void GetAllPhysicalQuestionsFromCourse_CourseMustExist() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = "0"};

			// Act
			RepeatedPhysicalQuestion result = _questionService.GetAllPhysicalQuestionsFromCourse(courseRequest,null).Result;
 
			// Assert
			Assert.False(result.Result.Succeeded);
		}

		[Test]
		public void RequestSessionQuestionsWithoutAnswers_Success() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = _dataForTest.Session1.Id};
			int amountOfQuestions = _dataForTest.Db.Questions.ToList().FindAll(
				q => q.OwningSession.Id == _dataForTest.Session1.Id && q.GetType() == typeof(Question)).Count;
			
			// Act
			RepeatedQuestionNoAnswer result = _questionService.RequestSessionQuestionsWithoutAnswers(sessionRequestById,null).Result;

			//Assert 
			Assert.True(result.Result.Succeeded);
			Assert.AreEqual(amountOfQuestions,result.Questions.Count);

		}
		
		[Test]
		public void RequestSessionQuestionsWithoutAnswers_SessionMustExist() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = 0};
			
			// Act
			RepeatedQuestionNoAnswer result = _questionService.RequestSessionQuestionsWithoutAnswers(sessionRequestById,null).Result;
			
			//Assert 
			Assert.False(result.Result.Succeeded);
			
		}
		
		[Test]
		public void RequestSessionQuestionsWithoutAnswers_IncludesCorrectQuestions() {
			// Arrange
			var sessionRequestById = new SessionRequestById(){SessionId = _dataForTest.Session1.Id};
			
			// Act
			RepeatedQuestionNoAnswer result = _questionService.RequestSessionQuestionsWithoutAnswers(sessionRequestById,null).Result;
			
			//Assert 
			Assert.AreEqual(_dataForTest.Question1.Title, result.Questions[0].Title);
			Assert.AreEqual(_dataForTest.Question1.ToPostableReply().Content, result.Questions[0].Postable.Content);
			Assert.AreEqual(_dataForTest.Question1.ToPostableReply().PostDate, result.Questions[0].Postable.PostDate);
			Assert.AreEqual(_dataForTest.Question1.ToPostableReply().Id, result.Questions[0].Postable.Id);
			Assert.AreEqual(_dataForTest.Question1.ToPostableReply().Poster.UserId, result.Questions[0].Postable.Poster.UserId);
			Assert.AreEqual(_dataForTest.Question1.ToPostableReply().Tags, result.Questions[0].Postable.Tags);
			
			Assert.AreEqual(_dataForTest.Question2.Title, result.Questions[1].Title);
			Assert.AreEqual(_dataForTest.Question2.ToPostableReply().Content, result.Questions[1].Postable.Content);
			Assert.AreEqual(_dataForTest.Question2.ToPostableReply().PostDate, result.Questions[1].Postable.PostDate);
			Assert.AreEqual(_dataForTest.Question2.ToPostableReply().Id, result.Questions[1].Postable.Id);
			Assert.AreEqual(_dataForTest.Question2.ToPostableReply().Poster.UserId, result.Questions[1].Postable.Poster.UserId);
			Assert.AreEqual(_dataForTest.Question2.ToPostableReply().Tags, result.Questions[1].Postable.Tags);
		}

		[Test]
		public void RequestCreateQuestion_Succeed() {
			// Arrange
			var createQuestionRequest = new CreateQuestionRequest() {Title = "cqrTitle", PostDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), 
				Poster = _dataForTest.User1.ToUserReplySafe(), Content = "cqrContent", SessionId = _dataForTest.Session1.Id};
			int amountOfQuestions = _dataForTest.Db.Questions.ToList().FindAll(
				q => q.OwningSession.Id == _dataForTest.Session1.Id && q.GetType() == typeof(Question)).Count;

			// Act
			RequestResult result = _questionService.RequestCreateQuestion(createQuestionRequest,null).Result;
			int amountOfQuestionsAfter = _dataForTest.Db.Questions.ToList().FindAll(
				q => q.OwningSession.Id == _dataForTest.Session1.Id && q.GetType() == typeof(Question)).Count;

			// Assert
			Assert.True(result.Succeeded);
			Assert.AreEqual(amountOfQuestions+1, amountOfQuestionsAfter);
		}
		
		[Test]
		public void RequestCreateQuestion_UserMustExist() {
			// Arrange
			var createQuestionRequest = new CreateQuestionRequest() {Title = "cqrTitle", PostDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), 
				Poster = new IdentityUser(){Id = "0"}.ToUserReplySafe(), Content = "cqrContent", SessionId = _dataForTest.Session1.Id};
			int amountOfQuestions = _dataForTest.Db.Questions.ToList().FindAll(
				q => q.OwningSession.Id == _dataForTest.Session1.Id && q.GetType() == typeof(Question)).Count;

			// Act
			RequestResult result = _questionService.RequestCreateQuestion(createQuestionRequest,null).Result;
			int amountOfQuestionsAfter = _dataForTest.Db.Questions.ToList().FindAll(
				q => q.OwningSession.Id == _dataForTest.Session1.Id && q.GetType() == typeof(Question)).Count;

			// Assert
			Assert.False(result.Succeeded);
			Assert.AreEqual(amountOfQuestions, amountOfQuestionsAfter);
		}
		
		[Test]
		public void RequestCreateQuestion_SessionMustExist() {
			// Arrange
			var createQuestionRequest = new CreateQuestionRequest() {Title = "cqrTitle", PostDate = Timestamp.FromDateTime(DateTime.Now.ToUniversalTime()), 
				Poster = _dataForTest.User1.ToUserReplySafe(), Content = "cqrContent", SessionId = 0};
			int amountOfQuestions = _dataForTest.Db.Questions.ToList().FindAll(
				q => q.OwningSession.Id == _dataForTest.Session1.Id && q.GetType() == typeof(Question)).Count;

			// Act
			RequestResult result = _questionService.RequestCreateQuestion(createQuestionRequest,null).Result;
			int amountOfQuestionsAfter = _dataForTest.Db.Questions.ToList().FindAll(
				q => q.OwningSession.Id == _dataForTest.Session1.Id && q.GetType() == typeof(Question)).Count;

			// Assert
			Assert.False(result.Succeeded);
			Assert.AreEqual(amountOfQuestions, amountOfQuestionsAfter);
		}
		
		
	}
	

}
