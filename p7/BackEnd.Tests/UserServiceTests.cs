﻿using System.Linq;
using BackEnd.Database;
using BackEnd.Services;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using NUnit.Framework;

namespace BackEnd.Tests {
	public class UserServiceTests {
		private DataForTest _dataForTest;
		private UserService _userService;

		[SetUp]
		public void SetUp() {
			_dataForTest?.Dispose();
			_dataForTest = new DataForTest();

			var userManager = new UserManager<IdentityUserCustom>(new UserStore<IdentityUserCustom>(_dataForTest.Db), 
				null,null,null,null,new UpperInvariantLookupNormalizer(), null, null,null);

			_userService = new UserService(userManager);
		}

		[Test]
		public void RequestUserById_Succeed() {
			// Arrange
			var userRequestById = new UserRequestById(){UserId = _dataForTest.User1.Id};

			// Act
			UserReply result = _userService.RequestUserById(userRequestById, null).Result;
			
			// Assert
			Assert.True(result.Result.Succeeded);
		}
		
		[Test]
		public void RequestUserById_FindRightUser() {
			// Arrange
			var userRequestById = new UserRequestById(){UserId = _dataForTest.User1.Id};
			
			// Act
			UserReply result = _userService.RequestUserById(userRequestById, null).Result;
			
			// Assert
			Assert.AreEqual(_dataForTest.User1.Id, result.User.UserId);

		}
		
		[Test]
		public void RequestUserById_UserMustExist() {
			// Arrange
			var userRequestById = new UserRequestById(){UserId = "0"};
			
			// Act
			UserReply result = _userService.RequestUserById(userRequestById, null).Result;
			
			// Assert
			Assert.False(result.Result.Succeeded);
		}

		[Test]
		public void RequestCreateUser_Succeed() {
			// Arrange
			var userCreateRequest = new UserCreateRequest(){Email = "mail@mail.com", IsConfirmed = true, 
				PasswordHash = "fdgdfg2", UserId = "0", Username = "testuserName"};
			
			// Act
			RequestResult result = _userService.RequestCreateUser(userCreateRequest, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RequestCreateUser_CreatesUserInDb() {
			// Arrange
			var userCreateRequest = new UserCreateRequest(){Email = "mail@mail.com", IsConfirmed = true, 
				PasswordHash = "fdgdfg2", UserId = "0", Username = "testuserName"};
			int amountOfUsers = _dataForTest.Db.Users.ToList().Count;
			
			// Act
			RequestResult result = _userService.RequestCreateUser(userCreateRequest, null).Result;
			int amountOfUsersAfter = _dataForTest.Db.Users.ToList().Count;

			// Assert
			Assert.AreEqual(amountOfUsers +1, amountOfUsersAfter);
		}
		
		[Test]
		public void RequestCreateUser_EmailMustBeUnique() {
			// Arrange
			var userCreateRequest = new UserCreateRequest(){Email = _dataForTest.User2.Email, IsConfirmed = true, 
				PasswordHash = "fdgdfg2", UserId = "0", Username = "testuserName"};
			int amountOfUsers = _dataForTest.Db.Users.ToList().Count;
			
			// Act
			RequestResult result = _userService.RequestCreateUser(userCreateRequest, null).Result;
			int amountOfUsersAfter = _dataForTest.Db.Users.ToList().Count;

			// Assert
			Assert.False(result.Succeeded);
			Assert.AreEqual(amountOfUsers, amountOfUsersAfter);
		}


		[Test]
		public void RequestUserByEmail_Succeed() {
			// Arrange 
			var userRequestByEmail = new UserRequestByEmail(){Email = _dataForTest.User1.Email};
			
			// Act
			UserReply result = _userService.RequestUserByEmail(userRequestByEmail, null).Result;

			// Assert
			Assert.True(result.Result.Succeeded);
		}
		
		[Test]
		public void RequestUserByEmail_FindRightUser() {
			// Arrange 
			var userRequestByEmail = new UserRequestByEmail(){Email = _dataForTest.User1.Email};
			
			// Act
			UserReply result = _userService.RequestUserByEmail(userRequestByEmail, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.User1.ToUserReplySafe(), result.User);
		}
		
		[Test]
		public void RequestUserByEmail_UserMustExist() {
			// Arrange 
			var userRequestByEmail = new UserRequestByEmail(){Email = "invalidEmail@mail.com"};
			
			// Act
			UserReply result = _userService.RequestUserByEmail(userRequestByEmail, null).Result;

			// Assert
			Assert.False(result.Result.Succeeded);
		}

		[Test]
		public void RequestSetUserName_Succeed() {
			// Arrange
			var userNameChangeRequest = new UserNameChangeRequest(){UserId = _dataForTest.User1.Id, Username = "newUsername"};
			
			// Act
			RequestResult result = _userService.RequestSetUserName(userNameChangeRequest, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RequestSetUserName_SavesUsername() {
			// Arrange
			var newUsername = "newUsername";
			var userNameChangeRequest = new UserNameChangeRequest(){UserId = _dataForTest.User1.Id, Username = newUsername};
			
			// Act
			RequestResult result = _userService.RequestSetUserName(userNameChangeRequest, null).Result;

			// Assert
			Assert.AreEqual(newUsername, _dataForTest.Db.Users.Find(_dataForTest.User1.Id).UserName);
		}
		
		[Test]
		public void RequestSetUserName_UserMustExist() {
			// Arrange
			var userNameChangeRequest = new UserNameChangeRequest(){UserId = "0", Username = "newUsername"};
			
			// Act
			RequestResult result = _userService.RequestSetUserName(userNameChangeRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}
		
		
		//////////////
		[Test]
		public void RequestSetEmail_Succeed() {
			// Arrange
			var userEmailChangeRequest = new UserEmailChangeRequest(){UserId = _dataForTest.User1.Id, Email = "newEmail@email.com"};
			
			// Act
			RequestResult result = _userService.RequestSetEmail(userEmailChangeRequest, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RRequestSetEmail_SavesEmail() {
			// Arrange
			var newUserEmail = "newEmail@email.com";
			var userEmailChangeRequest = new UserEmailChangeRequest(){UserId = _dataForTest.User1.Id, Email = newUserEmail};
			
			// Act
			RequestResult result = _userService.RequestSetEmail(userEmailChangeRequest, null).Result;

			// Assert
			Assert.AreEqual(newUserEmail, _dataForTest.Db.Users.Find(_dataForTest.User1.Id).Email);
		}
		
		[Test]
		public void RequestSetEmail_UserMustExist() {
			// Arrange
			var userEmailChangeRequest = new UserEmailChangeRequest(){UserId = "0",Email = "newEmail@email.com"};
			
			// Act
			RequestResult result = _userService.RequestSetEmail(userEmailChangeRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}


		[Test]
		public void RequestUserByName_Succeed() {
			// Arrange
			var userRequestByName = new UserRequestByName(){UserName = _dataForTest.User1.UserName};
			
			// Act
			UserReply result = _userService.RequestUserByName(userRequestByName, null).Result;

			// Assert
			Assert.True(result.Result.Succeeded);
		}
		
		[Test]
		public void RequestUserByName_FindRightUser() {
			// Arrange
			var userRequestByName = new UserRequestByName(){UserName = _dataForTest.User1.UserName};
			
			// Act
			UserReply result = _userService.RequestUserByName(userRequestByName, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.User1.Id, result.User.UserId);
		}
		
		[Test]
		public void RequestUserByName_UserMustExist() {
			// Arrange
			var userRequestByName = new UserRequestByName(){UserName = "nonExistingUserName"};
			
			// Act
			UserReply result = _userService.RequestUserByName(userRequestByName, null).Result;

			// Assert
			Assert.False(result.Result.Succeeded);
		}

		[Test]
		public void RequestAllUsers_ReturnsAllUsers() {
			// Arrange
			int amountOfUsers = _dataForTest.Db.Users.ToList().Count;

			// Act
			UserListReply result = _userService.RequestAllUsers(null, null).Result;

			// Assert
			Assert.AreEqual(amountOfUsers, result.Users.Count);
		}
		
		[Test]
		public void RequestAllUsers_ReturnedUsersAreRight() {
			// Arrange
			int amountOfUsers = _dataForTest.Db.Users.ToList().Count;

			// Act
			UserListReply result = _userService.RequestAllUsers(null, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.User1.ToUserReplySafe(), result.Users[0]);
			Assert.AreEqual(_dataForTest.User2.ToUserReplySafe(), result.Users[1]);
		}

		[Test]
		public void RequestUserUpdate_Succeed() {
			// Arrange
			IdentityUserCustom user1Updated = _dataForTest.User1;
			user1Updated.UserName = "newUserName";
			var userReply = new UserReply(){User = user1Updated.ToUserReplySafe()};
			
			// Act
			RequestResult result = _userService.RequestUserUpdate(userReply, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RequestUserUpdate_SavesUpdate() {
			// Arrange
			IdentityUserCustom user1Updated = _dataForTest.User1;
			var newUserName = "newUserName";
			user1Updated.UserName = newUserName;
			var userReply = new UserReply(){User = user1Updated.ToUserReplySafe()};
			
			// Act
			RequestResult result = _userService.RequestUserUpdate(userReply, null).Result;

			// Assert
			Assert.AreEqual(newUserName, _dataForTest.Db.Users.Find(_dataForTest.User1.Id).UserName);
		}
		
		[Test]
		public void RequestUserUpdate_UserMustExist() {
			// Arrange
			IdentityUserCustom user1Updated = _dataForTest.User1;
			user1Updated.Id = "0";
			var userReply = new UserReply(){User = user1Updated.ToUserReplySafe()};
			
			// Act
			RequestResult result = _userService.RequestUserUpdate(userReply, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void RequestUserDeletion_Succeed() {
			// Arrange
			var userRequestById = new UserRequestById(){UserId = _dataForTest.User1.Id};
			
			// Act
			RequestResult result = _userService.RequestUserDeletion(userRequestById, null).Result;
			
			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RequestUserDeletion_RemovesUser() {
			// Arrange
			var userRequestById = new UserRequestById(){UserId = _dataForTest.User1.Id};
			int amountOfUsers = _dataForTest.Db.Users.ToList().Count;
			
			// Act
			RequestResult result = _userService.RequestUserDeletion(userRequestById, null).Result;
			int amountOfUsersAfter = _dataForTest.Db.Users.ToList().Count;
			
			// Assert
			Assert.AreEqual(amountOfUsers-1, amountOfUsersAfter);
		}
		
		[Test]
		public void RequestUserDeletion_UserMustExist() {
			// Arrange
			var userRequestById = new UserRequestById(){UserId = "0"};
			int amountOfUsers = _dataForTest.Db.Users.ToList().Count;
			
			// Act
			RequestResult result = _userService.RequestUserDeletion(userRequestById, null).Result;
			int amountOfUsersAfter = _dataForTest.Db.Users.ToList().Count;
			
			// Assert
			Assert.AreEqual(amountOfUsers, amountOfUsersAfter);
			Assert.False(result.Succeeded);
		}
		
		/// <summary>
		/// ////////
		/// </summary>
		
		[Test]
		public void RequestUserUpdateSafe_Succeed() {
			// Arrange
			IdentityUserCustom user1Updated = _dataForTest.User1;
			user1Updated.UserName = "newUserName";
			var userReplySafe = user1Updated.ToUserReplySafe();

			// Act
			RequestResult result = _userService.RequestUserUpdateSafe(userReplySafe, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}
		
		[Test]
		public void RequestUserUpdateSafe_SavesUpdate() {
			// Arrange
			IdentityUserCustom user1Updated = _dataForTest.User1;
			var newUserName = "newUserName";
			user1Updated.UserName = newUserName;
			var userReplySafe = user1Updated.ToUserReplySafe();
			
			// Act
			RequestResult result = _userService.RequestUserUpdateSafe(userReplySafe, null).Result;

			// Assert
			Assert.AreEqual(newUserName, _dataForTest.Db.Users.Find(_dataForTest.User1.Id).UserName);
		}
		
		[Test]
		public void RequestUserUpdateSafe_UserMustExist() {
			// Arrange
			IdentityUserCustom user1Updated = _dataForTest.User1;
			user1Updated.Id = "0";
			var userReplySafe = user1Updated.ToUserReplySafe();
			
			// Act
			RequestResult result = _userService.RequestUserUpdateSafe(userReplySafe, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}
		
	}
}