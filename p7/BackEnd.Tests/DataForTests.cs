﻿using System;
using System.Collections.Generic;
using BackEnd.Database;
using Common.Data;
using Microsoft.AspNetCore.Identity;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace BackEnd.Tests {
	public class DataForTest : IDisposable {
		private const string InMemoryConnectionString = "DataSource=:memory:";
		private readonly SqliteConnection _connection;
		public readonly DatabaseContext Db;

		// Mocked objects
		public IdentityUserCustom User1;
		public IdentityUserCustom User2;
		public Session Session1;
		public Session Session2;
		public CourseDb Course1;
		public Question Question1;
		public Question Question2;
		public PhysicalQuestion PhysicalQuestion1;
		public PhysicalQuestion PhysicalQuestion2;
		public IdentityRole StaffRole;
		public IdentityRole NonStaffRole;
		public IdentityRole Course1LectureRole;
		public IdentityRole Course1StudentRole;
		public IdentityUserRole<string> User1UserRole;
		public IdentityUserRole<string> User2UserRole;
		public IdentityUserRole<string> User1UserRoleCourse1;
		public IdentityUserRole<string> User2UserRoleCourse1;

		public DataForTest() {
			_connection = new SqliteConnection(InMemoryConnectionString);
			_connection.Open();
			var options = new DbContextOptionsBuilder<DatabaseContext>()
				.UseSqlite(_connection)
				.Options;
			Db = new DatabaseContext(options);
			Db.Database.EnsureCreated();
			PopulateDb();
		}

		private void PopulateDb() {
			// Users
			User1 = new IdentityUserCustom(){Id = "1", Email = "1@1.com", NormalizedEmail = "1@1.COM", UserName = "userName1", 
				NormalizedUserName = "USERNAME1", LockoutEnabled = false, EmailConfirmed = true, PasswordHash = "yteryfdh"};
			User2 = new IdentityUserCustom(){Id = "2", Email = "2@2.com", NormalizedEmail = "2@2.COM", UserName = "userName2", 
				NormalizedUserName = "USERNAME2",LockoutEnabled = false, EmailConfirmed = true, PasswordHash = "yteryfdh"};
			var users = new List<IdentityUserCustom>() {User1,User2};
			Db.Users.AddRange(users);

			// Course
			Course1 = new CourseDb("course1Code", "course1Name", 2000, false, User1, "course1Subtitle");
			Course1.ParticipantsDb.Add(User2);
			Session1 = new Session("session1Name", DateTime.Now, DateTime.Now) {Id = 1};
			Session2 = new Session("session2Name", DateTime.Now, DateTime.Now) {Id = 2};
			var sessions = new List<Session>() {Session1, Session2};
			Course1.Sessions.AddRange(sessions);
			Db.Courses.Add(Course1);

			// Questions
			Question1 = new Question(User2, "question1Title", "question1Content", Session1);
			Question2 = new Question(User2, "question2Title", "question2Content", Session1);
			var questions = new List<Question>() {Question1, Question2};
			Db.Questions.AddRange(questions);

			// Physical Questions
			PhysicalQuestion1 = new PhysicalQuestion("physicalQuestion1Title", "physicalQuestion1Location", User2,
				"physicalQuestion1Content", Session1);
			PhysicalQuestion2 = new PhysicalQuestion("physicalQuestion2Title", "physicalQuestion2Location", User2,
				"physicalQuestion2Content", Session1);
			var physicalQuestions = new List<PhysicalQuestion>() {PhysicalQuestion1, PhysicalQuestion2};
			Db.PhysicalQuestions.AddRange(physicalQuestions);

			// Roles
			StaffRole = new IdentityRole() {Id = "StaffRole", Name = "Staff", NormalizedName = "STAFF"};
			StaffRole.NormalizedName = StaffRole.Name.ToUpper();
			NonStaffRole = new IdentityRole() {Id = "NonStaffRole", Name = "Non-Staff", NormalizedName = "NON-STAFF"};
			Course1LectureRole = new IdentityRole()
				{Id = "Course1LectureRole", Name = "Lecturer-" + Course1.CourseCode};
			Course1LectureRole.NormalizedName = Course1LectureRole.Name.ToUpper();
			Course1StudentRole = new IdentityRole() {Id = "Course1StudentRole", Name = "Student-" + Course1.CourseCode};
			Course1StudentRole.NormalizedName = Course1StudentRole.Name.ToUpper();
			var roles = new List<IdentityRole>() {StaffRole, NonStaffRole, Course1LectureRole, Course1StudentRole};
			Db.Roles.AddRange(roles);

			// UserRoles
			User1UserRole = new IdentityUserRole<string>() {RoleId = "StaffRole", UserId = User1.Id};
			User2UserRole = new IdentityUserRole<string>() {RoleId = "NonStaffRole", UserId = User2.Id};
			User1UserRoleCourse1 = new IdentityUserRole<string>() {RoleId = "Course1LectureRole", UserId = User1.Id};
			User2UserRoleCourse1 = new IdentityUserRole<string>() {RoleId = "Course1StudentRole", UserId = User2.Id};
			var userRoles = new List<IdentityUserRole<string>>()
				{User1UserRole, User2UserRole, User1UserRoleCourse1, User2UserRoleCourse1};
			Db.UserRoles.AddRange(userRoles);

			// Save the changes
			Db.SaveChanges();
		}

		public void Dispose() {
			_connection.Close();
		}
	}

	public class TestDataTest : DataForTest {
		[Test]
		public void VerifyTestDb() {
			Assert.True(Db.Database.CanConnectAsync().Result);
		}
	}
}
