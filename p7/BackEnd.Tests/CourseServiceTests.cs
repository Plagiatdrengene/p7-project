﻿using System;
using System.Linq;
using BackEnd.Database;
using BackEnd.Services;
using Common.Protos.Course;
using Common.Protos.Session;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;

namespace BackEnd.Tests {
	public class CourseServiceTests {
		private DataForTest _dataForTest;
		private CourseService _courseService;

		[SetUp]
		public void SetUp() {
			_dataForTest = new DataForTest();

			var userManager = new UserManager<IdentityUserCustom>(new UserStore<IdentityUserCustom>(_dataForTest.Db),
				null, null, null, null, new UpperInvariantLookupNormalizer(), null, null,
				new NullLogger<UserManager<IdentityUserCustom>>());
			var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_dataForTest.Db),
				null, new UpperInvariantLookupNormalizer(), null, new NullLogger<RoleManager<IdentityRole>>());

			_courseService = new CourseService(_dataForTest.Db, userManager, roleManager);
		}

		[TearDown]
		public void TearDown() {
			_dataForTest?.Db.Database.EnsureDeleted();
			_dataForTest?.Dispose();
		}
		

		[Test]
		public void RequestCourse_Succeed() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			CourseReply result = _courseService.RequestCourse(courseRequest, null).Result;

			// Assert
			Assert.True(result.TinyCourse.Result.Succeeded);
		}

		[Test]
		public void RequestCourse_ReceivesRightInfo() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			CourseReply result = _courseService.RequestCourse(courseRequest, null).Result;

			Console.WriteLine(result.Participants.Count);
			// Assert
			Assert.AreEqual(_dataForTest.User2.Id, result.Participants[0].UserId);
		}

		[Test]
		public void RequestCourse_CourseMustExist() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = "0"};

			// Act
			CourseReply result = _courseService.RequestCourse(courseRequest, null).Result;

			// Assert
			Assert.False(result.TinyCourse.Result.Succeeded);
		}

		[Test]
		public void RequestTinyCourse_Succeed() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			CourseTinyReply result = _courseService.RequestTinyCourse(courseRequest, null).Result;

			// Assert
			Assert.True(result.Result.Succeeded);
		}

		[Test]
		public void RequestTinyCourse_ReceivesRightInfo() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			CourseTinyReply result = _courseService.RequestTinyCourse(courseRequest, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.User1.Id, result.Creator.UserId);
			Assert.AreEqual(_dataForTest.Course1.CourseCode, result.CourseCode);
			Assert.AreEqual(_dataForTest.Course1.Name, result.Name);
			Assert.AreEqual(_dataForTest.Course1.SubTitle, result.SubTitle);
			Assert.AreEqual(_dataForTest.Course1.Tags, result.Tags);
			Assert.AreEqual(_dataForTest.Course1.Year, result.Year);
			Assert.AreEqual(_dataForTest.Course1.IsFall, result.IsFall);
			Assert.AreEqual(_dataForTest.Course1.SupportedLanguages, result.Languages);
		}

		[Test]
		public void RequestTinyCourse_CourseMustExist() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = "0"};

			// Act
			CourseTinyReply result = _courseService.RequestTinyCourse(courseRequest, null).Result;

			// Assert
			Assert.False(result.Result.Succeeded);
		}

		[Test]
		public void CreateCourse_Succeed() {
			// Arrange
			var courseTinyReply = new CourseTinyReply() {
				CourseCode = "coursecodeTest", Name = "nameTest",
				SubTitle = "subtitleTest", Tags = {new TagReply() {TagName = "tagTest"}}, Year = 2020, IsFall = false,
				Creator = _dataForTest.User1.ToUserReplySafe(),
				Languages = {new LanguageReply() {LanguageName = "languageTest"}}
			};
			int amountOfCourses = _dataForTest.Db.Courses.ToList().Count;

			// Act
			CourseCreatedReply result = _courseService.CreateCourse(courseTinyReply, null).Result;
			int amountOfCoursesAfter = _dataForTest.Db.Courses.ToList().Count;

			// Assert
			Assert.True(result.Result.Succeeded);
			Assert.AreEqual(amountOfCourses + 1, amountOfCoursesAfter);
		}

		[Test]
		public void CreateCourse_UserMustExist() {
			// Arrange
			var courseTinyReply = new CourseTinyReply() {
				CourseCode = "coursecodeTest", Name = "nameTest",
				SubTitle = "subtitleTest", Tags = {new TagReply() {TagName = "tagTest"}}, Year = 2020, IsFall = false,
				Creator = new UserReplySafe() {UserId = "0"},
				Languages = {new LanguageReply() {LanguageName = "languageTest"}}
			};
			int amountOfCourses = _dataForTest.Db.Courses.ToList().Count;

			// Act
			var result = _courseService.CreateCourse(courseTinyReply, null).Result;
			int amountOfCoursesAfter = _dataForTest.Db.Courses.ToList().Count;

			// Assert
			//Assert.False(result.Result.Succeeded);
			Assert.AreEqual(amountOfCourses, amountOfCoursesAfter);
		}

		[Test]
		public void IsUserPartOfCourse_SucceedIfUserIsInCourse() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = _dataForTest.User2.Id};

			// Act
			RequestResult result = _courseService.IsUserPartOfCourse(userCourseRequest, null).Result;

			// Assert
			Assert.True(result.Succeeded);
		}

		[Test]
		public void IsUserPartOfCourse_SucceedIfUserIsNotInCourse() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = _dataForTest.User1.Id};

			// Act
			RequestResult result = _courseService.IsUserPartOfCourse(userCourseRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void IsUserPartOfCourse_UserMustExist() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = "0"};

			// Act
			RequestResult result = _courseService.IsUserPartOfCourse(userCourseRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void IsUserPartOfCourse_CourseMustExist() {
			// Arrange
			var userCourseRequest = new UserCourseRequest() {CourseCode = "0", UserId = _dataForTest.User1.Id};

			// Act
			RequestResult result = _courseService.IsUserPartOfCourse(userCourseRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void GetAllCoursesThatUserIsPartOf_Succeed() {
			// Arrange
			var userRequestById = new UserRequestById() {UserId = _dataForTest.User2.Id};

			// Act
			RepeatedTinyCourse result = _courseService.GetAllCoursesThatUserIsPartOf(userRequestById, null).Result;

			// Assert
			Assert.True(result.Courses[0].Result.Succeeded);
		}

		[Test]
		public void GetAllCoursesThatUserIsPartOf_ReturnsAll() {
			// Arrange
			var userRequestById = new UserRequestById() {UserId = _dataForTest.User2.Id};
			int amountOfCoursesUser2PartOf = _dataForTest.Db.Courses.ToList()
				.FindAll(c => c.ParticipantsDb.Contains(_dataForTest.User2)).Count;

			// Act
			RepeatedTinyCourse result = _courseService.GetAllCoursesThatUserIsPartOf(userRequestById, null).Result;

			// Assert
			Assert.True(result.Courses[0].Result.Succeeded);
			Assert.AreEqual(amountOfCoursesUser2PartOf, result.Courses.Count);
		}

		[Test]
		public void GetAllCoursesThatUserIsPartOf_ReturnsRightCourse() {
			// Arrange
			var userRequestById = new UserRequestById() {UserId = _dataForTest.User2.Id};

			// Act
			RepeatedTinyCourse result = _courseService.GetAllCoursesThatUserIsPartOf(userRequestById, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.Course1.CourseCode, result.Courses[0].CourseCode);
			Assert.AreEqual(_dataForTest.Course1.Name, result.Courses[0].Name);
			Assert.AreEqual(_dataForTest.Course1.SubTitle, result.Courses[0].SubTitle);
			Assert.AreEqual(_dataForTest.Course1.Tags, result.Courses[0].Tags);
			Assert.AreEqual(_dataForTest.Course1.Year, result.Courses[0].Year);
			Assert.AreEqual(_dataForTest.Course1.IsFall, result.Courses[0].IsFall);
			Assert.AreEqual(_dataForTest.Course1.Creator.Id, result.Courses[0].Creator.UserId);
			Assert.AreEqual(_dataForTest.Course1.SupportedLanguages, result.Courses[0].Languages);
		}

		[Test]
		public void GetAllCoursesThatUserIsPartOf_UserMustExist() {
			// Arrange
			var userRequestById = new UserRequestById() {UserId = "0"};

			// Act
			RepeatedTinyCourse result = _courseService.GetAllCoursesThatUserIsPartOf(userRequestById, null).Result;

			// Assert
			Assert.False(result.Result.Succeeded);
		}

		[Test]
		public void RemoveUserFromCourse_Succeed() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = _dataForTest.User2.Id};

			// Act
			RequestResult result = _courseService.RemoveUserFromCourse(userCourseRequest, null).Result;

			// Assert 
			Assert.True(result.Succeeded);
		}

		[Test]
		public void RemoveUserFromCourse_RemovesUserFromCourse() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = _dataForTest.User2.Id};
			int amountOfCoursesUser2IsPartOf = _dataForTest.Db.Courses.ToList()
				.FindAll(c => c.ParticipantsDb.Contains(_dataForTest.User2)).Count;

			// Act
			RequestResult result = _courseService.RemoveUserFromCourse(userCourseRequest, null).Result;
			int amountOfCoursesUser2IsPartOfAfter = _dataForTest.Db.Courses.ToList()
				.FindAll(c => c.ParticipantsDb.Contains(_dataForTest.User2)).Count;

			// Assert 
			Assert.AreEqual(amountOfCoursesUser2IsPartOf - 1, amountOfCoursesUser2IsPartOfAfter);
		}

		[Test]
		public void RemoveUserFromCourse_UserMustExist() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = "0"};

			// Act
			RequestResult result = _courseService.RemoveUserFromCourse(userCourseRequest, null).Result;

			// Assert 
			Assert.False(result.Succeeded);
		}

		[Test]
		public void RemoveUserFromCourse_CourseMustExist() {
			// Arrange
			var userCourseRequest = new UserCourseRequest() {CourseCode = "0", UserId = _dataForTest.User2.Id};

			// Act
			RequestResult result = _courseService.RemoveUserFromCourse(userCourseRequest, null).Result;

			// Assert 
			Assert.False(result.Succeeded);
		}

		[Test]
		public void RemoveUserFromCourse_UserMustParticipateInCourse() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = _dataForTest.User1.Id};

			// Act
			RequestResult result = _courseService.RemoveUserFromCourse(userCourseRequest, null).Result;

			// Assert 
			Assert.False(result.Succeeded);
		}

		[Test]
		public void JoinCourse_Succeed() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = _dataForTest.User1.Id};
			int amountOfCoursesUser1ParticipateIn = _dataForTest.Db.Courses.ToList()
				.FindAll(c => c.ParticipantsDb.Contains(_dataForTest.User1)).Count;

			// Act
			RequestResult result = _courseService.JoinCourse(userCourseRequest, null).Result;
			int amountOfCoursesUser1ParticipateInafter = _dataForTest.Db.Courses.ToList()
				.FindAll(c => c.ParticipantsDb.Contains(_dataForTest.User1)).Count;

			// Assert
			Assert.True(result.Succeeded);
			Assert.AreEqual(amountOfCoursesUser1ParticipateIn + 1, amountOfCoursesUser1ParticipateInafter);
		}

		[Test]
		public void JoinCourse_CourseMustExist() {
			// Arrange
			var userCourseRequest = new UserCourseRequest() {CourseCode = "0", UserId = _dataForTest.User1.Id};

			// Act
			RequestResult result = _courseService.JoinCourse(userCourseRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void JoinCourse_UserMustExist() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = "0"};

			// Act
			RequestResult result = _courseService.JoinCourse(userCourseRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void JoinCourse_UserCanNotAlreadyBeParticipant() {
			// Arrange
			var userCourseRequest = new UserCourseRequest()
				{CourseCode = _dataForTest.Course1.CourseCode, UserId = _dataForTest.User2.Id};

			// Act
			RequestResult result = _courseService.JoinCourse(userCourseRequest, null).Result;

			// Assert
			Assert.False(result.Succeeded);
		}

		[Test]
		public void GetCourseSessions_Succeed() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			RepeatedSessionReply result = _courseService.GetCourseSessions(courseRequest, null).Result;

			// Assert
			Assert.True(result.Request.Succeeded);
		}

		[Test]
		public void GetCourseSessions_ReturnsAllSessions() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};
			int amountOfSessionsForSession1 = _dataForTest.Db.Sessions.ToList()
				.FindAll(s => s.Course.CourseCode == _dataForTest.Course1.CourseCode).Count;

			// Act
			RepeatedSessionReply result = _courseService.GetCourseSessions(courseRequest, null).Result;

			// Assert
			Assert.AreEqual(amountOfSessionsForSession1, result.Sessions.Count);
		}

		[Test]
		public void GetCourseSessions_ReturnsRightSessions() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			RepeatedSessionReply result = _courseService.GetCourseSessions(courseRequest, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.Session1.Id, result.Sessions[0].SessionId);
			Assert.AreEqual(_dataForTest.Session1.Name, result.Sessions[0].Name);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.Session1.StartDate.ToUniversalTime()),
				result.Sessions[0].StartDate);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.Session1.EndDate.ToUniversalTime()),
				result.Sessions[0].EndDate);

			Assert.AreEqual(_dataForTest.Session2.Id, result.Sessions[1].SessionId);
			Assert.AreEqual(_dataForTest.Session2.Name, result.Sessions[1].Name);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.Session2.StartDate.ToUniversalTime()),
				result.Sessions[1].StartDate);
			Assert.AreEqual(Timestamp.FromDateTime(_dataForTest.Session2.EndDate.ToUniversalTime()),
				result.Sessions[1].EndDate);
		}

		[Test]
		public void GetCourseSessions_CourseMustExist() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = "0"};

			// Act
			RepeatedSessionReply result = _courseService.GetCourseSessions(courseRequest, null).Result;

			// Assert
			Assert.False(result.Request.Succeeded);
		}

		[Test]
		public void GetAllUsersPartOfCourse_Succeed() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			RepeatedUserCourseRole result = _courseService.GetAllUsersPartOfCourse(courseRequest, null).Result;

			// Assert
			Assert.True(result.Result.Succeeded);
		}

		[Test]
		public void GetAllUsersPartOfCourse_FindAllUsers() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};
			int amountOfParticipantsInCourse1 =
				_dataForTest.Db.Courses.Find(_dataForTest.Course1.CourseCode).ParticipantsDb.Count;

			// Act
			RepeatedUserCourseRole? result = _courseService.GetAllUsersPartOfCourse(courseRequest, null).Result;

			// Assert
			Assert.AreEqual(amountOfParticipantsInCourse1, result.Users.Count);
		}

		[Test]
		public void GetAllUsersPartOfCourse_FindCorrectUsers() {
			// Arrange
			var courseRequest = new CourseRequest() {CourseCode = _dataForTest.Course1.CourseCode};

			// Act
			RepeatedUserCourseRole result = _courseService.GetAllUsersPartOfCourse(courseRequest, null).Result;

			// Assert
			Assert.AreEqual(_dataForTest.User2.Id, result.Users[0].User.UserId);
		}
	}
}