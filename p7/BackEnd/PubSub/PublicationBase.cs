﻿using System;
using System.Text;
using Common.PubSub;
using Grpc.Core;
using KubeMQ.SDK.csharp.Events;
using Newtonsoft.Json;
using Channel = KubeMQ.SDK.csharp.Events.Channel;

namespace BackEnd.PubSub {
	public abstract class PublicationBase {
		public PubSubInformation PubSubInformation { get; }
		private readonly Channel? _channel;

		protected PublicationBase(string channelName, string clientId, string kubeMqServerAddress) {
			PubSubInformation = new PubSubInformation(channelName, clientId, kubeMqServerAddress);
			if (!Startup.IsContainerized) return;
			_channel = new Channel(new ChannelParameters {
				ChannelName = PubSubInformation.ChannelName,
				ClientID = PubSubInformation.ClientId,
				KubeMQAddress = PubSubInformation.KubeMqServerAddress
			});
		}

		protected void SendMessage(object objectToSend) {
			if (!Startup.IsContainerized) return;
			string json = JsonConvert.SerializeObject(objectToSend, Formatting.Indented);
			try {
				Result? result = _channel.SendEvent(new Event {
					Body = Encoding.Default.GetBytes(json)
				});
				if (!result.Sent) {
					Console.Error.WriteLine($"Could not send single message:{result.Error}");
				}
			}
			catch (RpcException ex) {
				Console.Error.WriteLine("Failed at sending message, printing error message:\n" + ex.Message);
			}
		}
	}
}