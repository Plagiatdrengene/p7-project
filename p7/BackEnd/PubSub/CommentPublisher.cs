﻿using Common.PubSub.Data;

namespace BackEnd.PubSub {
	public class CommentPublisher : PublicationBase {
		public CommentPublisher() : base("Comment", "CommentBackend",
			"kubemq-cluster-grpc.kubemq.svc.cluster.local:50000") { }

		public void CommentCreated(string courseCode, int commentId) {
			var update = new PostableUpdate(courseCode, UpdateType.Created, commentId);
			SendMessage(update);
		}
	}
}