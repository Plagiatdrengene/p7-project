﻿using Common.PubSub.Data;

namespace BackEnd.PubSub {
	public class QuestionAnswerPublisher : PublicationBase {
		public QuestionAnswerPublisher() : base("QuestionAnswer", "QuestionAnswerBackend",
			"kubemq-cluster-grpc.kubemq.svc.cluster.local:50000") { }

		public void QuestionAnswerCreated(string courseCode, int questionAnswerId) {
			var update = new PostableUpdate(courseCode, UpdateType.Created, questionAnswerId);
			SendMessage(update);
		}
	}
}