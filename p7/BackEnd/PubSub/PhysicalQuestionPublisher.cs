﻿using Common.PubSub.Data;

namespace BackEnd.PubSub {
	public class PhysicalQuestionPublisher : PublicationBase {
		public PhysicalQuestionPublisher() : base("PhysicalQuestion", "PhysicalQuestionBackend",
			"kubemq-cluster-grpc.kubemq.svc.cluster.local:50000") { }

		public void QuestionCreated(string courseCode, int questionId) {
			var update = new PostableUpdate(courseCode, UpdateType.Created, questionId);
			SendMessage(update);
		}
	}
}