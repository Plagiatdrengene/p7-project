﻿using Common.PubSub.Data;

namespace BackEnd.PubSub {
	public class QuestionPublisher : PublicationBase {
		public QuestionPublisher() : base("Question", "QuestionBackend",
			"kubemq-cluster-grpc.kubemq.svc.cluster.local:50000") { }

		public void QuestionCreated(string courseCode, int questionId) {
			var update = new PostableUpdate(courseCode, UpdateType.Created, questionId);
			SendMessage(update);
		}
	}
}