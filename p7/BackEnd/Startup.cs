using System;
using BackEnd.Database;
using BackEnd.PubSub;
using BackEnd.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;


namespace BackEnd {
	public class Startup {
		public IConfiguration Configuration { get; }

		public static bool IsContainerized => Environment.GetEnvironmentVariable("IS_CONTAINERIZED") != null;

		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		// This method gets called by the runtime. Use this method to add services to the container.
		// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
		public void ConfigureServices(IServiceCollection services) {
			services.AddGrpc();
			services.AddDbContext<DatabaseContext>(builder => {
					string? dbEnvVar = Environment.GetEnvironmentVariable("DATABASE_CONNECTION_STRING");
					builder.UseNpgsql(dbEnvVar ?? Configuration.GetConnectionString("DefaultConnection"));
				}
			);
			services.AddAuthentication(options => {
				options.DefaultScheme = IdentityConstants.ApplicationScheme;
				options.DefaultSignInScheme = IdentityConstants.ExternalScheme;
			}).AddIdentityCookies();


			services.AddDefaultIdentity<IdentityUserCustom>(options => {
				options.Stores.MaxLengthForKeys = 128;
				options.SignIn.RequireConfirmedAccount = false;
				options.SignIn.RequireConfirmedEmail = false;
				options.Password.RequireDigit = false;
				options.Password.RequireUppercase = false;
				options.Password.RequireNonAlphanumeric = false;
				options.Password.RequiredLength = 4;
			}).AddRoles<IdentityRole>().AddEntityFrameworkStores<DatabaseContext>();


			RegisterPubs(services);
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env, DatabaseContext context,
			RoleManager<IdentityRole> roleManager) {
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			}

			context.Database.EnsureCreated();

			if (!roleManager.RoleExistsAsync("Staff").Result) {
				IdentityResult? _ = roleManager.CreateAsync(new IdentityRole("Staff")).Result;
			}
			//new DbPopulator(context).Populate();


			app.UseRouting();

			app.UseEndpoints(endpoints => {
				endpoints.MapGrpcService<UserService>();
				endpoints.MapGrpcService<CourseService>();
				endpoints.MapGrpcService<QuestionService>();
				endpoints.MapGrpcService<RoleService>();
				endpoints.MapGrpcService<ClaimsService>();
				endpoints.MapGrpcService<TokenService>();
				endpoints.MapGrpcService<SessionService>();

				endpoints.MapGet("/",
					async localContext => {
						await localContext.Response.WriteAsync(
							"Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client");
					});
			});
		}

		private static void RegisterPubs(IServiceCollection services) {
			if (!IsContainerized)
				Console.WriteLine(
					"WARNING: Not running Containerized! Pub/Sub won't work.\nTo set up correctly see: https://gitlab.com/swp7/k8sconfigs");
			services.AddSingleton<PhysicalQuestionPublisher>();
			services.AddSingleton<QuestionPublisher>();
			services.AddSingleton<QuestionAnswerPublisher>();
			services.AddSingleton<CommentPublisher>();
		}
	}
}