﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace BackEnd.Database {
	public sealed class IdentityUserCustom : IdentityUser {
		public ICollection<CourseDb> JoinedCourses { get; } = new List<CourseDb>();
	}
}