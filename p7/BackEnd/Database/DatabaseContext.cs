using Common.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Database {
	public class DatabaseContext : IdentityDbContext {
		public DbSet<Question> Questions { get; init; }
		public DbSet<PhysicalQuestion> PhysicalQuestions { get; init; }
		public DbSet<MultiMediaItem> MultiMediaItems { get; init; }
		public DbSet<QuestionAnswer> Answers { get; init; }
		public DbSet<Comment> Comments { get; init; }
		public DbSet<UpVote> UpVotes { get; init; }
		public DbSet<Tag> Tags { get; init; }
		public DbSet<CourseDb> Courses { get; init; }
		public DbSet<Language> Languages { get; init; }
		public DbSet<Session> Sessions { get; init; }

		public DatabaseContext(DbContextOptions<DatabaseContext> options)
			: base(options) { }

		protected override void OnModelCreating(ModelBuilder builder) {
			builder.Entity<IdentityUserCustom>().HasMany(p => p.JoinedCourses)
				.WithMany(course => course.ParticipantsDb);
			builder.Entity<IdentityUserCustom>(entity => { entity.HasIndex(e => e.NormalizedEmail).IsUnique(); });
			builder.Entity<CourseDb>().HasOne(course => course.Creator);
			
			builder.Entity<Postable>().ToTable("Postable");
			builder.Entity<Question>().ToTable("Questions");
			builder.Entity<PhysicalQuestion>().ToTable("PhysicalQuestions");
			builder.Entity<Comment>().ToTable("Comments");
			builder.Entity<QuestionAnswer>().ToTable("QuestionAnswers");
			
			base.OnModelCreating(builder);

			foreach (var entityType in builder.Model.GetEntityTypes()) {
				string? table = entityType.GetTableName();
				if (table.StartsWith("AspNet")) {
					entityType.SetTableName(table.Substring(6));
				}
			}
		}
	}
}