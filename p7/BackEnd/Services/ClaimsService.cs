﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using BackEnd.Database;
using Common.Protos.Claims;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Google.Protobuf.Collections;
using Grpc.Core;
using Microsoft.AspNetCore.Identity;

namespace BackEnd.Services {
	public sealed class ClaimsService : ClaimsHandler.ClaimsHandlerBase {
		private readonly UserManager<IdentityUserCustom> _userManager;

		public ClaimsService(UserManager<IdentityUserCustom> userManager) {
			_userManager = userManager;
		}

		public override async Task<ClaimsReply> RequestUserClaims(UserRequestById request, ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.UserId);
			IList<Claim>? claims = await _userManager.GetClaimsAsync(user);

			RepeatedField<ProtoClaim> result = new();
			foreach (Claim claim in claims) {
				var protoClaim = claim.ToProtoClaim();
				if (protoClaim != null)
					result.Add(protoClaim);
				else {
					await Console.Error.WriteLineAsync("Failed to create claim.");
				}
			}

			return new ClaimsReply {Claims = {result}};
		}

		public override async Task<RequestResult>
			RequestAddClaims(UserClaimsRequest request, ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.User.UserId);
			IdentityResult result =
				await _userManager.AddClaimsAsync(user, request.Claims.Select(claim => claim.ToClaim()));
			return result.ToRequestResult();
		}

		public override async Task<RequestResult> RequestReplaceClaim(ClaimsReplaceRequest request,
			ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.User.UserId);
			//TODO I'm unsure if this will work since the claims are not the tracked instances. 
			return (await _userManager.ReplaceClaimAsync(user, request.OldClaim.ToClaim(), request.NewClaim.ToClaim()))
				.ToRequestResult();
		}

		public override async Task<RequestResult> RequestRemoveClaims(UserClaimsRequest request,
			ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.User.UserId);
			return (await _userManager.RemoveClaimsAsync(user, request.Claims.Select(claim => claim.ToClaim())))
				.ToRequestResult();
		}

		public override async Task<UserListReply> RequestUsersForClaim(ProtoClaim request, ServerCallContext context) {
			IEnumerable<UserReplySafe?> users = (await _userManager.GetUsersForClaimAsync(request.ToClaim())).Select(
				custom => custom.ToUserReplySafe());
			return new UserListReply {Users = {users}};
		}
	}
}