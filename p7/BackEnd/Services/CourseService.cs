﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Database;
using Common.Protos.Course;
using Common.Protos.Session;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Core;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Services {
	public sealed class CourseService : CourseHandler.CourseHandlerBase {
		private readonly DatabaseContext _applicationDbContext;
		private readonly UserManager<IdentityUserCustom> _userManager;
		private readonly RoleManager<IdentityRole> _roleManager;

		public CourseService(DatabaseContext applicationDbContext, UserManager<IdentityUserCustom> userManager,
			RoleManager<IdentityRole> roleManager) {
			_applicationDbContext = applicationDbContext;
			_userManager = userManager;
			_roleManager = roleManager;
		}

		public override async Task<CourseReply> RequestCourse(CourseRequest request, ServerCallContext context) {
			CourseDb course = await _applicationDbContext.Courses.Include(db => db.Creator)
				.Include(db => db.Participants)
				.Include(db => db.Sessions).Include(db => db.SupportedLanguages).Include(db => db.Tags)
				.FirstOrDefaultAsync(db => db.CourseCode == request.CourseCode);
			CourseReply courseReply = course.ToCourseReply();
			return courseReply;
		}

		public override async Task<CourseTinyReply>
			RequestTinyCourse(CourseRequest request, ServerCallContext context) {
			CourseDb course = await _applicationDbContext.Courses.Include(db => db.Creator)
				.Include(db => db.Participants)
				.Include(db => db.Sessions).Include(db => db.SupportedLanguages).Include(db => db.Tags)
				.FirstOrDefaultAsync(db => db.CourseCode == request.CourseCode);
			var courseReply = course.ToTinyCourseReply();
			return courseReply;
		}

		public override async Task<CourseCreatedReply>
			CreateCourse(CourseTinyReply request, ServerCallContext context) {
			IdentityUserCustom? user = _applicationDbContext.Users.FirstOrDefault(identityUser =>
				identityUser.Id == request.Creator.UserId) as IdentityUserCustom;
			if (user == null) {
				return new CourseCreatedReply {Result = RequestResult.FailRequest("User does not exist in DB")};
			}

			var course = new CourseDb(KeyGenerator.RandomString(4), request.Name, request.Year, request.IsFall,
				user, request.SubTitle);

			foreach (TagReply tagReply in request.Tags) {
				var tag = tagReply.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					course.Tags.Add(tag);
				}
			}

			foreach (LanguageReply languageReply in request.Languages) {
				var language = languageReply.ToLanguage();
				if (language != null && !string.IsNullOrEmpty(language.LanguageName)) {
					course.SupportedLanguages.Add(language);
				}
			}

			await _applicationDbContext.Courses.AddAsync(course);
			user.JoinedCourses.Add(course);


			int success = await _applicationDbContext.SaveChangesAsync();
			return new CourseCreatedReply
				{Result = new RequestResult {Succeeded = success != 0}, CourseCode = course.CourseCode};
		}

		public override async Task<RequestResult> IsUserPartOfCourse(UserCourseRequest request,
			ServerCallContext context) {
			var user = (IdentityUserCustom?) await _applicationDbContext.Users
				.Include(identityUser => ((IdentityUserCustom) identityUser).JoinedCourses).FirstOrDefaultAsync(
					identityUser => identityUser.Id == request.UserId);

			if (user is null)
				return new RequestResult {Errors = {"No such user."}, Succeeded = false};

			bool isInCourse = user.JoinedCourses.Any(db => db.CourseCode == request.CourseCode);
			return new RequestResult {Succeeded = isInCourse};
		}

		public override async Task<RepeatedTinyCourse> GetAllCoursesThatUserIsPartOf(UserRequestById request,
			ServerCallContext context) {
			var user = (IdentityUserCustom?) await _applicationDbContext.Users.FindAsync(request.UserId);
			if (user is null) {
				return new RepeatedTinyCourse() {Result = RequestResult.FailRequest("No such user.")};
			}

			HashSet<string>? courseCodes = _applicationDbContext.Users.Cast<IdentityUserCustom>()
				.Include(custom => custom.JoinedCourses)
				.First(custom => custom.Id == request.UserId).JoinedCourses.Select(db => db.CourseCode).ToHashSet();

			List<CourseDb> courses = _applicationDbContext.Courses.Include(db => db.Tags)
				.Include(db => db.Creator)
				.Include(db => db.SupportedLanguages)
				.Where(db => courseCodes.Contains(db.CourseCode)).ToList();


			return new RepeatedTinyCourse
				{Result = RequestResult.SucceededRequest(), Courses = {courses.Select(db => db.ToTinyCourseReply())}};
		}

		public override async Task<RequestResult> RemoveUserFromCourse(UserCourseRequest request,
			ServerCallContext context) {
			CourseDb? courseDb = _applicationDbContext.Courses.Include(db => db.ParticipantsDb)
				.FirstOrDefault(db => db.CourseCode == request.CourseCode);

			IdentityUserCustom? user = _applicationDbContext.Users.FirstOrDefault(identityUser =>
				identityUser.Id == request.UserId) as IdentityUserCustom;
			if (user is null) {
				return new RequestResult {Succeeded = false, Errors = {$"No user with the id {request.UserId} found."}};
			}

			if (courseDb != null) {
				IdentityUserCustom? dbUser = courseDb.ParticipantsDb.Find(user => user.Id == request.UserId);

				if (dbUser != null) {
					if (dbUser.Id == courseDb.Creator.Id) {
						return RequestResult.FailRequest("The creator of a course can't un-enroll from the course.");
					}

					courseDb.ParticipantsDb.Remove(dbUser);
					await _applicationDbContext.SaveChangesAsync();
					if (await _userManager.IsInRoleAsync(dbUser, "Student-" + courseDb.CourseCode))
						await _userManager.RemoveFromRoleAsync(dbUser, "Student-" + courseDb.CourseCode);

					if (await _userManager.IsInRoleAsync(dbUser, "TeachingAssistant-" + courseDb.CourseCode))
						await _userManager.RemoveFromRoleAsync(dbUser, "TeachingAssistant-" + courseDb.CourseCode);

					if (await _userManager.IsInRoleAsync(dbUser, "Lecturer-" + courseDb.CourseCode))
						await _userManager.RemoveFromRoleAsync(dbUser, "Lecturer-" + courseDb.CourseCode);
				}
				else {
					return new RequestResult {Succeeded = false, Errors = {"User is not part of course."}};
				}
			}
			else {
				return new RequestResult {Succeeded = false, Errors = {"Course does not exist"}};
			}


			return new RequestResult {Succeeded = true};
		}

		public override async Task<RequestResult> JoinCourse(UserCourseRequest request, ServerCallContext context) {
			var course = _applicationDbContext.Courses.Include(db => db.ParticipantsDb)
				.FirstOrDefault(db => db.CourseCode == request.CourseCode);

			if (course == null) {
				return new RequestResult {Errors = {"Course does not exist"}, Succeeded = false};
			}

			IdentityUser? user =
				_applicationDbContext.Users.FirstOrDefault(identityUser => identityUser.Id == request.UserId);
			if (!(user is IdentityUserCustom)) {
				return new RequestResult {Succeeded = false, Errors = {"User does not exist"}};
			}

			if (course.ParticipantsDb.Any(custom => custom.Id == request.UserId)) {
				return RequestResult.FailRequest("User is already part of course.");
			}

			string role = "Student-" + course.CourseCode;
			if (!await _roleManager.RoleExistsAsync(role)) {
				await _roleManager.CreateAsync(new IdentityRole(role));
			}

			await _userManager.AddToRoleAsync((IdentityUserCustom) user, role);


			if (course.ParticipantsDb.Contains(user)) {
				return new RequestResult {Errors = {"You are already part of this course"}, Succeeded = false};
			}

			course.ParticipantsDb.Add(user as IdentityUserCustom);
			await _applicationDbContext.SaveChangesAsync();

			return new RequestResult {Succeeded = true};
		}

		public override async Task<RepeatedSessionReply> GetCourseSessions(CourseRequest request,
			ServerCallContext context) {
			CourseDb course = await _applicationDbContext.Courses
				.Include(db => db.Sessions)
				.FirstOrDefaultAsync(db => db.CourseCode == request.CourseCode);
			if (course is null) {
				return new RepeatedSessionReply
					{Request = new RequestResult() {Succeeded = false, Errors = {"Course does not exist"}}};
			}

			List<SessionProto?> sessions = course.Sessions.Select(session => session.ToSessionReply()).ToList();
			return new RepeatedSessionReply {Request = new RequestResult() {Succeeded = true}, Sessions = {sessions}};
		}

		public override async Task<RepeatedUserCourseRole> GetAllUsersPartOfCourse(CourseRequest request,
			ServerCallContext context) {
			CourseDb course = await _applicationDbContext.Courses.FindAsync(request.CourseCode);

			if (course is null) {
				return new RepeatedUserCourseRole()
					{Result = RequestResult.FailRequest("No course with that ID could be found.")};
			}
			
			List<IList<IdentityUserCustom>> tasks = new List<IList<IdentityUserCustom>>() {
				await _userManager.GetUsersInRoleAsync("Student-" + request.CourseCode),
				await _userManager.GetUsersInRoleAsync("TeachingAssistant-" + request.CourseCode),
				await _userManager.GetUsersInRoleAsync("Lecturer-" + request.CourseCode)
			};
			var result = new RepeatedUserCourseRole();

			result.Users.AddRange(tasks[0].Select(custom => new UserCourseRole()
				{RoleName = "Student-" + request.CourseCode, User = custom.ToUserReplySafe()}));
			result.Users.AddRange(tasks[1].Select(custom => new UserCourseRole()
				{RoleName = "TeachingAssistant-" + request.CourseCode, User = custom.ToUserReplySafe()}));
			result.Users.AddRange(tasks[2].Select(custom => new UserCourseRole()
				{RoleName = "Lecturer-" + request.CourseCode, User = custom.ToUserReplySafe()}));
			// Removes creator
			result.Users.Remove(result.Users.First(role => role.User.UserId == course.Creator.Id));

			result.Result = RequestResult.SucceededRequest();

			return result;
		}
	}
}