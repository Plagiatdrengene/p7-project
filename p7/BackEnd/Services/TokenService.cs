using System;
using System.Threading.Tasks;
using BackEnd.Database;
using Common.Protos.Token;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Core;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Services {
	public sealed class TokenService : TokenHandler.TokenHandlerBase {
		private readonly UserManager<IdentityUserCustom> _userManager;
		private readonly DatabaseContext _databaseContext;

		public TokenService(UserManager<IdentityUserCustom> userManager, DatabaseContext databaseContext) {
			_userManager = userManager;
			_databaseContext = databaseContext;
		}

		public override async Task<RequestResult> RequestAddToken(TokenRequest request, ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.TokenUserId);

			if (user is null) {
				return RequestResult.FailRequest($"Token add failed - no user with id {request.TokenUserId} found.");
			}

			var token = new IdentityUserToken<string>() {
				Name = request.TokenName, Value = request.TokenValue, LoginProvider = request.TokenLoginProvider,
				UserId = request.TokenUserId
			};
			await _databaseContext.UserTokens.AddAsync(token);
			int result = await _databaseContext.SaveChangesAsync();
			return result == 0
				? RequestResult.FailRequest($"Token add for userId {user.Id} failed.")
				: RequestResult.SucceededRequest();
		}

		public override async Task<RequestResult> RequestRemoveToken(TokenRequest request,
			ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.TokenUserId);

			if (user is null) {
				return RequestResult.FailRequest($"Token remove failed - no user with id {request.TokenUserId} found.");
			}


			IdentityUserToken<string>? token = await _databaseContext.UserTokens.FirstAsync(userToken =>
				userToken.Name == request.TokenName && userToken.LoginProvider == request.TokenLoginProvider &&
				userToken.UserId == request.TokenUserId);
			_databaseContext.UserTokens.Remove(token);
			int result = await _databaseContext.SaveChangesAsync();
			return result == 0
				? RequestResult.FailRequest($"Token remove for userId {user.Id} failed.")
				: RequestResult.SucceededRequest();
		}

		public override async Task<TokenReply> RequestFindToken(TokenRequestFind request, ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.TokenUserId);

			if (user is null) {
				return new TokenReply {
					Result = RequestResult.FailRequest($"Token look-up failed with userId {request.TokenUserId}")
				};
			}

			IdentityUserToken<string>? token = await _databaseContext.UserTokens.FirstOrDefaultAsync(userToken =>
				userToken.Name == request.TokenName && userToken.LoginProvider == request.TokenLoginProvider &&
				userToken.UserId == request.TokenUserId);
			if (token == null)
				return new TokenReply() {Result = RequestResult.SucceededRequest()};
			var result = token.ToTokenReply();
			if (result == null)
				return new TokenReply {Result = RequestResult.FailRequest("Couldn't convert token to token reply.")};
			result.Result = RequestResult.SucceededRequest();
			return result;
		}
	}
}