﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Database;
using Common.Protos.Roles;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Core;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Services {
	public sealed class RoleService : RoleHandler.RoleHandlerBase {
		private readonly UserManager<IdentityUserCustom> _userManager;
		private readonly DatabaseContext _databaseContext;
		private readonly RoleManager<IdentityRole> _roleManager;

		public RoleService(UserManager<IdentityUserCustom> userManager, DatabaseContext databaseContext,
			RoleManager<IdentityRole> roleManager) {
			_userManager = userManager;
			_databaseContext = databaseContext;
			_roleManager = roleManager;
		}

		public override async Task<UserIsInRoleReply>
			RequestIsInRole(UserRoleRequestByName request, ServerCallContext context) {
			IdentityUserCustom user = await _userManager.FindByIdAsync(request.UserId);
			if (user is null) {
				return new UserIsInRoleReply
					{IsInRole = false, RequestResult = RequestResult.FailRequest("User does not exist")};
			}

			bool isInRole = await _userManager.IsInRoleAsync(user, request.NormalizedRoleName);
			return new UserIsInRoleReply {IsInRole = isInRole, RequestResult = RequestResult.SucceededRequest()};
		}
		
		public override async Task<UserRole> RequestUserRole(UserRoleRequestById request, ServerCallContext context) {
			IdentityUserRole<string> role = await _databaseContext.UserRoles
				.Where(userRole => userRole.UserId == request.UserId && userRole.RoleId == request.RoleId)
				.FirstOrDefaultAsync();
			if (role is null) {
				return new UserRole() {Result = RequestResult.FailRequest("Failed to find user or role.")};
			}

			return new UserRole {
				Result = RequestResult.SucceededRequest(),
				RoleId = role.RoleId, UserId = role.UserId
			};
		}

		public override async Task<UserListReply> RequestUsersInRole(RoleRequestByName request,
			ServerCallContext context) {
			IList<IdentityUserCustom>? usersInRole = await _userManager.GetUsersInRoleAsync(request.NormalizedName);
			return new UserListReply {Users = {usersInRole.Select(custom => custom.ToUserReplySafe())}};
		}

		public override async Task<RequestResult> RequestAddToRole(UserRoleRequestByName request,
			ServerCallContext context) {
			IdentityUserCustom user = await _userManager.FindByIdAsync(request.UserId);
			if (user is null) {
				return RequestResult.FailRequest("User does not exist");
			}

			IdentityRole role = await _roleManager.FindByNameAsync(request.NormalizedRoleName);
			if (role is null) {
				return RequestResult.FailRequest("Role does not exist");
			}

			return (await _userManager.AddToRoleAsync(user, request.NormalizedRoleName)).ToRequestResult();
		}

		public override async Task<RequestResult> RequestRemoveFromRole(UserRoleRequestByName request,
			ServerCallContext context) {
			IdentityUserCustom user = await _userManager.FindByIdAsync(request.UserId);
			if (user is null) {
				return RequestResult.FailRequest("User does not exist");
			}

			IdentityRole role = await _roleManager.FindByNameAsync(request.NormalizedRoleName);
			if (role is null) {
				return RequestResult.FailRequest("Role does not exist");
			}

			return (await _userManager.RemoveFromRoleAsync(user, request.NormalizedRoleName)).ToRequestResult();
		}

		public override async Task<RoleNameList> RequestGetRoles(UserRequestById request, ServerCallContext context) {
			IdentityUserCustom user = await _userManager.FindByIdAsync(request.UserId);
			if (user is null) {
				return new RoleNameList();
			}

			IList<string>? roles = await _userManager.GetRolesAsync(user);
			return new RoleNameList {Roles = {roles.Select(name => new RoleNameRequest() {Name = name})}};
		}

		public override async Task<RequestResult> RequestCreateRole(RoleReply request, ServerCallContext context) {
			IdentityRole role = await _roleManager.FindByIdAsync(request.Id);
			if (role is not null) {
				return new RequestResult() {Succeeded = false, Errors = {"There all ready exist a role with that id"}};
			}

			IdentityResult? result = await _roleManager.CreateAsync(new IdentityRole() {
				Id = request.Id, Name = request.Name, ConcurrencyStamp = request.ConcurrencyStamp,
				NormalizedName = request.NormalizedName
			});
			return result.ToRequestResult();
		}

		public override async Task<RequestResult> RequestUpdateRole(RoleReply request, ServerCallContext context) {
			IdentityRole role = await _roleManager.FindByIdAsync(request.Id);
			if (role is null) {
				return new RequestResult() {Succeeded = false, Errors = {"There is no role with that id"}};
			}

			role.Name = request.Name;
			role.NormalizedName = request.NormalizedName;
			role.ConcurrencyStamp = request.ConcurrencyStamp;
			IdentityResult? result = await _roleManager.UpdateAsync(role);

			return result.ToRequestResult();
		}

		public override async Task<RequestResult>
			RequestDeleteRole(RoleRequestById request, ServerCallContext context) {
			IdentityRole? role = await _roleManager.FindByIdAsync(request.RoleId);
			if (role is null) {
				return new RequestResult() {Succeeded = false, Errors = {"No role with that id"}};
			}

			IdentityResult? result = await _roleManager.DeleteAsync(role);
			return result.ToRequestResult();
		}

		public override async Task<RoleReply> RequestRoleById(RoleRequestById request, ServerCallContext context) {
			IdentityRole? role = await _roleManager.FindByIdAsync(request.RoleId);
			if (role is null) {
				return new RoleReply()
					{RequestResult = new RequestResult() {Succeeded = false, Errors = {"No role with that id"}}};
			}

			var roleReply = new RoleReply() {
				Id = role.Id, Name = role.Name, ConcurrencyStamp = role.ConcurrencyStamp,
				NormalizedName = role.NormalizedName, RequestResult = new RequestResult() {Succeeded = true}
			};
			return roleReply;
		}

		public override async Task<RoleReply?> RequestRoleByName(RoleRequestByName request, ServerCallContext context) {
			IdentityRole? role = await _roleManager.FindByNameAsync(request.NormalizedName);
			if (role is null) {
				return new RoleReply()
					{RequestResult = new RequestResult() {Succeeded = false, Errors = {"No role with that id"}}};
			}

			var roleReply = new RoleReply {
				Id = role.Id, Name = role.Name, ConcurrencyStamp = role.ConcurrencyStamp,
				NormalizedName = role.NormalizedName, RequestResult = new RequestResult() {Succeeded = true}
			};
			return roleReply;
		}

		public override async Task<RoleList> RequestAllRoles(Empty request, ServerCallContext context) {
			IQueryable<RoleReply> roles = _roleManager.Roles.Select(role => new RoleReply() {
				Id = role.Id, Name = role.Name, ConcurrencyStamp = role.ConcurrencyStamp,
				NormalizedName = role.NormalizedName
			});
			return new RoleList() {Roles = {roles}};
		}
	}
}