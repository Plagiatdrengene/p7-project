using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Database;
using Common.Protos.User;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Core;
using Microsoft.AspNetCore.Identity;

namespace BackEnd.Services {
	public sealed class UserService : UserHandler.UserHandlerBase {
		private readonly UserManager<IdentityUserCustom> _userManager;

		public UserService(UserManager<IdentityUserCustom> userManager) {
			_userManager = userManager;
		}

		public override async Task<UserReply> RequestUserById(UserRequestById request, ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.UserId);

			if (user == null) {
				return new UserReply {
					Result = RequestResult.FailRequest($"No user with the id {request.UserId} found.")
				};
			}

			var userReply = user.ToUserReply();
			Debug.Assert(userReply != null, nameof(userReply) + " != null");
			userReply.Result = new RequestResult {Succeeded = true};
			return userReply;
		}

		public override async Task<RequestResult> RequestCreateUser(UserCreateRequest request,
			ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByEmailAsync(request.Email);
			if (user != null)
				return RequestResult.FailRequest($"A user with the email: {request.Email} already exists.");
			user = new IdentityUserCustom {
				Id = request.UserId,
				UserName = request.Username,
				NormalizedUserName = request.Username.ToUpper(),
				Email = request.Email,
				NormalizedEmail = request.Email.ToUpper(),
				PasswordHash = request.PasswordHash,
			};

			IdentityResult? result = await _userManager.CreateAsync(user);

			return result.ToRequestResult();
		}


		public override async Task<UserReply>
			RequestUserByEmail(UserRequestByEmail request, ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByEmailAsync(request.Email);

			if (user == null) {
				return new UserReply {
					Result = RequestResult.FailRequest($"No user found with the email: {request.Email}.")
				};
			}
			else {
				var userReply = user.ToUserReply();
				Debug.Assert(userReply != null, nameof(userReply) + " != null");
				userReply.Result = RequestResult.SucceededRequest();
				return userReply;
			}
		}

		public override async Task<RequestResult> RequestSetUserName(UserNameChangeRequest request,
			ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.UserId);
			if (user == null)
				return RequestResult.FailRequest($"No user with the id {request.UserId} was found.");
			user.UserName = request.Username;

			IdentityResult? result = await _userManager.UpdateAsync(user);
			await _userManager.UpdateNormalizedUserNameAsync(user);

			return result.ToRequestResult();
		}

		public override async Task<RequestResult> RequestSetEmail(UserEmailChangeRequest request,
			ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.UserId);
			if (user == null)
				return RequestResult.FailRequest($"No user with the id {request.UserId} was found.");

			user.Email = request.Email;
			IdentityResult? result = await _userManager.UpdateAsync(user);
			await _userManager.UpdateNormalizedEmailAsync(user);

			return result.ToRequestResult();
		}

		public override async Task<UserReply> RequestUserByName(UserRequestByName request, ServerCallContext context) {
			IdentityUser? user = await _userManager.FindByNameAsync(request.UserName);

			if (user == null) {
				return new UserReply {
					Result = RequestResult.FailRequest($"No user with the user name {request.UserName} was found.")
				};
			}

			var userReply = user.ToUserReply();
			if (userReply == null) return new UserReply {User = null};
			userReply.Result = RequestResult.SucceededRequest();
			return userReply;
		}

		public override async Task<UserListReply> RequestAllUsers(Empty request, ServerCallContext context) {
			return await Task.FromResult(new UserListReply
				{Users = {_userManager.Users.Select(user => user.ToUserReplySafe())}});
		}

		public override async Task<RequestResult> RequestUserUpdate(UserReply request, ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.User.UserId);
			if (user is null)
				return RequestResult.FailRequest($"Failed to update the user: {request.User.Email}.");
			user.Email = request.User.Email;
			user.UserName = request.User.Username;
			user.EmailConfirmed = request.User.IsConfirmed;
			user.LockoutEnabled = request.User.IsLockedOut;
			user.PasswordHash = request.PasswordHash;

			return (await _userManager.UpdateAsync(user)).ToRequestResult();
		}

		public override async Task<RequestResult> RequestUserDeletion(UserRequestById request,
			ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.UserId);
			if (user is null) {
				return new RequestResult(){Succeeded = false, Errors = {"No user with that id"}};
			}
			return (await _userManager.DeleteAsync(user)).ToRequestResult();
		}

		public override async Task<RequestResult> RequestUserUpdateSafe(UserReplySafe request,
			ServerCallContext context) {
			IdentityUserCustom? user = await _userManager.FindByIdAsync(request.UserId);
			if (user is null)
				return RequestResult.FailRequest($"Failed to update the user: {request.Email}.");
			user.Email = request.Email;
			user.UserName = request.Username;
			user.EmailConfirmed = request.IsConfirmed;
			user.LockoutEnabled = request.IsLockedOut;

			return (await _userManager.UpdateAsync(user)).ToRequestResult();
		}
	}
}