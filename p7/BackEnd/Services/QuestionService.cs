﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BackEnd.Database;
using BackEnd.PubSub;
using Common.Data;
using Common.Protos.Course;
using Common.Protos.PhysicalQuestion;
using Common.Protos.Session;
using Common.Protos.Utility;
using Common.Utility;
using Grpc.Core;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Formatters;
using Microsoft.EntityFrameworkCore;

namespace BackEnd.Services {
	public sealed class QuestionService : QuestionHandler.QuestionHandlerBase {
		private readonly DatabaseContext _applicationDbContext;
		private readonly PhysicalQuestionPublisher _physicalQuestionPublisher;
		private readonly QuestionPublisher _questionPublisher;
		private readonly QuestionAnswerPublisher _questionAnswerPublisher;
		private readonly CommentPublisher _commentPublisher;

		public QuestionService(DatabaseContext applicationDbContext,
			PhysicalQuestionPublisher physicalQuestionPublisher,
			QuestionPublisher questionPublisher, CommentPublisher commentPublisher,
			QuestionAnswerPublisher questionAnswerPublisher) {
			_applicationDbContext = applicationDbContext;
			_physicalQuestionPublisher = physicalQuestionPublisher;
			_questionPublisher = questionPublisher;
			_commentPublisher = commentPublisher;
			_questionAnswerPublisher = questionAnswerPublisher;
		}

		public override async Task<PhysicalQuestionCreatedReply> CreatePhysicalQuestion(
			PhysicalQuestionCreateRequest request, ServerCallContext context) {
			Session? session = await _applicationDbContext.Sessions.Include(session1 => session1.Course)
				.FirstOrDefaultAsync(session1 => session1.Id == request.SessionId);
			if (session is null) {
				return new PhysicalQuestionCreatedReply
					{Result = new RequestResult {Succeeded = false, Errors = {"The session ID was not found in DB"}}};
			}

			IdentityUser? user = await _applicationDbContext.Users.FindAsync(request.Question.User.UserId);

			var physicalQuestion = request.Question.ToPhysicalQuestion();
			if (physicalQuestion is null) {
				return new PhysicalQuestionCreatedReply {
					Result = new RequestResult {
						Succeeded = false, Errors = {"Failed to convert PhysicalQuestionCreationRequest to data model."}
					}
				};
			}

			if (user is null)
				return new PhysicalQuestionCreatedReply {
					Result = new RequestResult {
						Succeeded = false, Errors = {"No user found with that id."}
					}
				};

			physicalQuestion.Poster = user;
			await _applicationDbContext.PhysicalQuestions.AddAsync(physicalQuestion);
			physicalQuestion.OwningSession = session;
			await _applicationDbContext.PhysicalQuestions.AddAsync(physicalQuestion);
			int result = await _applicationDbContext.SaveChangesAsync();
			if (result == 0) {
				return new PhysicalQuestionCreatedReply {
					Result = new RequestResult {
						Succeeded = false, Errors =
							{"Could not add physical question to the database"}
					}
				};
			}

			_physicalQuestionPublisher.QuestionCreated(session.Course.CourseCode, physicalQuestion.Id);
			return new PhysicalQuestionCreatedReply {Result = new RequestResult {Succeeded = true}};
		}

		public override async Task<RequestResultWithId> RequestCreateComment(CreateQuestionAnswerCommentRequest request,
			ServerCallContext context) {
			var comment = request.ToComment();
			if (comment is null) {
				return new RequestResultWithId {
					Result = RequestResult.FailRequest("Could not convert Proto CreateQuestionRequest to Comment")
				};
			}

			comment.Poster = await _applicationDbContext.Users.FindAsync(request.Poster.UserId);
			comment.QuestionAnswer = await _applicationDbContext.Answers.Include(answer => answer.OwningSession)
				.ThenInclude(session => session.Course).FirstAsync(answer => answer.Id == request.BelongsTo);
			comment.OwningSession = comment.QuestionAnswer.OwningSession;


			await _applicationDbContext.Comments.AddAsync(comment);
			int result = await _applicationDbContext.SaveChangesAsync();

			if (result == 0)
				return new RequestResultWithId
					{Result = RequestResult.FailRequest("Failed to add the QuestionAnswer to the database.")};
			else {
				_commentPublisher.CommentCreated(comment.OwningSession.Course.CourseCode, comment.Id);
				return new RequestResultWithId {Result = RequestResult.SucceededRequest(), Id = comment.Id};
			}
		}

		public override async Task<QuestionsReplyWithAnswers?> RequestQuestionById(QuestionIdProto request,
			ServerCallContext context) {
			var result = _applicationDbContext.Questions
				.Include(question => question.Poster)
				.Where(question => question.Id == request.Id).ToList().FirstOrDefault();

			if (result is null) {
				return new QuestionsReplyWithAnswers
					{Result = RequestResult.FailRequest($"Could not get Question with Id #{request.Id} from database")};
			}

			result.QuestionAnswers = await _applicationDbContext.Answers
				.Include(answer => answer.Question)
				.Include(answer => answer.Poster)
				.Where(answer => answer.Question == result).ToListAsync();

			if (result.QuestionAnswers.Count <= 0)
				return result.ToQuestionReplyWithAnswers();

			foreach (QuestionAnswer questionAnswer in result.QuestionAnswers) {
				questionAnswer.Question = result;
				questionAnswer.Comments = await _applicationDbContext.Comments
					.Include(comment => comment.Poster)
					.Include(comment => comment.QuestionAnswer)
					.Where(comment => comment.QuestionAnswer == questionAnswer).ToListAsync();
				foreach (Comment comment in questionAnswer.Comments) {
					comment.QuestionAnswer = questionAnswer;
				}
			}

			return result.ToQuestionReplyWithAnswers();
		}

		public override async Task<RepeatedPhysicalQuestion> GetAllPhysicalQuestionsFromSession(
			SessionRequestById request,
			ServerCallContext context) {
			if (await _applicationDbContext.Sessions.FindAsync(request.SessionId) == null)
				return new RepeatedPhysicalQuestion() {Result = RequestResult.FailRequest("No session with that id.")};
			List<PhysicalQuestion>? res = await _applicationDbContext.PhysicalQuestions
				.Include(question => question.Poster)
				.Where(question => question.OwningSession.Id == request.SessionId).ToListAsync();

			return new RepeatedPhysicalQuestion {
				Questions = {res.Select(question => question.ToPhysicalQuestionProto())},
				Result = new RequestResult {Succeeded = true}
			};
		}

		public override async Task<RepeatedPhysicalQuestion> GetAllPhysicalQuestionsFromCourse(CourseRequest request,
			ServerCallContext context) {
			Course? course = await _applicationDbContext.Courses.FindAsync(request.CourseCode);
			if (course is null) {
				return new RepeatedPhysicalQuestion
					{Result = new RequestResult {Succeeded = false, Errors = {"The courseCode was not found in DB"}}};
			}


			List<PhysicalQuestion>? result = await _applicationDbContext.PhysicalQuestions
				.Include(question => question.OwningSession)
				.Include(question => question.Poster).Where(question =>
					question.OwningSession.Course.CourseCode == request.CourseCode).ToListAsync();


			return new RepeatedPhysicalQuestion {
				Questions = {result.Select(question => question.ToPhysicalQuestionProto())},
				Result = new RequestResult {Succeeded = true}
			};
		}

		public override async Task<RepeatedQuestionNoAnswer> RequestSessionQuestionsWithoutAnswers(
			SessionRequestById request, ServerCallContext context) {
			Session? session = await _applicationDbContext.Sessions.FindAsync(request.SessionId);
			if (session is null) {
				return new RepeatedQuestionNoAnswer
					{Result = new RequestResult {Succeeded = false, Errors = {"The session ID was not found in DB"}}};
			}

			List<Question> questions = _applicationDbContext.Questions
				.Include(question => question.Poster)
				.Where(question => question.OwningSession != null && question.OwningSession.Id == request.SessionId &&
				                   !(question is PhysicalQuestion)).ToList();
			return new RepeatedQuestionNoAnswer {
				Questions = {questions.Select(question => question.ToQuestionReplyNoAnswers())},
				Result = new RequestResult() {Succeeded = true}
			};
		}

		public override async Task<RequestResult> RequestCreateQuestion(CreateQuestionRequest request,
			ServerCallContext context) {
			Question? question = request.ToQuestion();
			if (question is null) {
				return RequestResult.FailRequest("Could not convert question create proto to question model");
			}

			Session? session = await _applicationDbContext.Sessions.FindAsync(request.SessionId);
			if (session is null) {
				return new RequestResult {Succeeded = false, Errors = {"The session ID was not found in DB"}};
			}

			if (request.Poster is null)
				return new RequestResult {Succeeded = false, Errors = {"No user provided."}};
			IdentityUser? user = await _applicationDbContext.Users.FindAsync(request.Poster.UserId);
			if (user is null)
				return new RequestResult {Succeeded = false, Errors = {"No user found with that id."}};
			question.Poster = user;

			question.OwningSession = await _applicationDbContext.Sessions.Include(session1 => session1.Course)
				.FirstAsync(session1 => session1.Id == request.SessionId);
			await _applicationDbContext.Questions.AddAsync(question);
			int result = await _applicationDbContext.SaveChangesAsync();


			if (result == 0)
				return RequestResult.FailRequest("Failed to add the QuestionAnswer to the database.");
			else {
				_questionPublisher.QuestionCreated(question.OwningSession.Course.CourseCode, question.Id);
				return RequestResult.SucceededRequest();
			}
		}

		public override async Task<RequestResultWithId> RequestCreateQuestionAnswer(
			CreateQuestionAnswerCommentRequest request,
			ServerCallContext context) {
			var questionAnswer = request.ToQuestionAnswer();
			if (questionAnswer is null) {
				return new RequestResultWithId {
					Result = RequestResult.FailRequest("Could not convert Proto-Request to QuestionAnswer")
				};
			}

			questionAnswer.Poster = await _applicationDbContext.Users.FindAsync(request.Poster.UserId);
			questionAnswer.Question = await _applicationDbContext.Questions.Include(question => question.OwningSession)
				.Include(question => question.OwningSession.Course)
				.FirstAsync(question => question.Id == request.BelongsTo);
			questionAnswer.OwningSession = questionAnswer.Question.OwningSession;
			await _applicationDbContext.Answers.AddAsync(questionAnswer);
			int result = await _applicationDbContext.SaveChangesAsync();


			if (result == 0)
				return new RequestResultWithId
					{Result = RequestResult.FailRequest("Failed to add the QuestionAnswer to the database.")};
			else {
				_questionAnswerPublisher.QuestionAnswerCreated(questionAnswer.OwningSession.Course.CourseCode,
					questionAnswer.Question.Id);
				return new RequestResultWithId {Result = RequestResult.SucceededRequest(), Id = questionAnswer.Id};
			}
		}

		public void Dispose() {
			_applicationDbContext.Dispose();
		}
	}
}