using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace Common.Data {
	public class UpVote {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required]
		public int Id { get; set; }

		public Question Question { get; set; }
		public IdentityUser User { get; set; }

		public UpVote(Question question, IdentityUser user) {
			Question = question;
			User = user;
		}

		[UsedImplicitly]
		private UpVote() {
			Question = null!;
			User = null!;
		}
	}
}