using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace Common.Data {
	public class TinyCourse {
		[Key] public string CourseCode { get; set; }
		[Required] public string Name { get; set; }
		public string SubTitle { get; set; }

		[Required] public List<Tag> Tags { get; set; } = new List<Tag>();
		[Required] public int Year { get; set; }
		[Required] public bool IsFall { get; set; }

		[Required] public IdentityUser Creator { get; set; }

		[Required] public List<Language> SupportedLanguages { get; set; } = new List<Language>();

		public TinyCourse(string name, int year, bool isFall, IdentityUser? user, string subTitle = "") {
			Name = name;
			SubTitle = subTitle;
			Year = year;
			IsFall = isFall;
			Creator = user ?? throw new NoNullAllowedException();
			CourseCode = "";
		}

		public TinyCourse(string courseCode, string name, int year, bool isFall, IdentityUser? user,
			string subTitle = "") :
			this(name, year, isFall, user, subTitle) {
			CourseCode = courseCode;
		}

		protected TinyCourse() {
			Name = "";
			SubTitle = "";
			CourseCode = "";
			Creator = null!;
		}
	}
}