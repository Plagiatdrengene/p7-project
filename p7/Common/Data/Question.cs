using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace Common.Data {
	public class Question : Postable {
		[Required] public string Title { get; set; }
		public bool IsSolved { get; set; }
		public bool IsUnderstood { get; set; }
		public bool IsBeingLookedAtByTA { get; set; }
		public List<QuestionAnswer> QuestionAnswers { get; set; } = new List<QuestionAnswer>();

		public Question(IdentityUser poster, string title, string content, Session? session) : base(poster,
			content, session) {
			PostDate = DateTime.Now;
			Title = title;
		}
		public Question(IdentityUser poster, string title, string content) : base(poster,
			content) {
			PostDate = DateTime.Now;
			Title = title;
		}

		[UsedImplicitly]
		public Question() : base(null!, "", null) {
			Title = "";
		}
	}
}