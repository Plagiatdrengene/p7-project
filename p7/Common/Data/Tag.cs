using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;

namespace Common.Data {
	public class Tag {
		[Key] [Required] public string TagName { get; set; }

		public Tag(string tagName) {
			TagName = tagName;
		}

		[UsedImplicitly]
		private Tag() {
			TagName = "";
		}
	}
}