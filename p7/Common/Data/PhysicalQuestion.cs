﻿using System;
using System.ComponentModel.DataAnnotations;
using JetBrains.Annotations;
using Microsoft.AspNetCore.Identity;

namespace Common.Data {
	public class PhysicalQuestion : Question {
		[Required] public string? Location { get; set; }

		[UsedImplicitly]
		public PhysicalQuestion() : base(null!, "", "", null!) {
			Location = "";
		}

		public PhysicalQuestion(string title, string location, IdentityUser poster,
			string content, Session session) : base(poster, title, content, session) {
			Title = title;
			PostDate = DateTime.Now;
			Location = location;
		}
		public PhysicalQuestion(string title, string location, IdentityUser poster,
			string content) : base(poster, title, content) {
			Title = title;
			PostDate = DateTime.Now;
			Location = location;
		}
	}
}