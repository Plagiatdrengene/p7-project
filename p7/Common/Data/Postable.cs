using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace Common.Data {
	public abstract class Postable {
		[Key]
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Required]
		public int Id { get; set; }

		[Required] public DateTime PostDate { get; set; }

		[Required] public IdentityUser? Poster { get; set; }
		public List<Tag> Tags { get; set; } = new List<Tag>();
		[Required] public string? Content { get; set; }

		[Required] public Session? OwningSession { get; set; }

		protected Postable(IdentityUser poster, string content, Session session) {
			PostDate = DateTime.Now;
			Poster = poster;
			Content = content;
			OwningSession = session;
		}

		protected Postable(IdentityUser poster, string content) {
			PostDate = DateTime.Now;
			Poster = poster;
			Content = content;
		}
	}
}