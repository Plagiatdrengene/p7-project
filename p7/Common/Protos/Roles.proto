﻿syntax = "proto3";
import "Protos/Util.proto";
import "Protos/User.proto";
option csharp_namespace = "Common.Protos.Roles";

service RoleHandler{
  //Role store methods
  rpc RequestCreateRole (RoleReply) returns (RequestResult);
  rpc RequestUpdateRole (RoleReply) returns (RequestResult);
  rpc RequestDeleteRole (RoleRequestById) returns (RequestResult);
  rpc RequestRoleById (RoleRequestById) returns (RoleReply);
  rpc RequestRoleByName (RoleRequestByName) returns (RoleReply);
  rpc RequestAllRoles (Empty) returns (RoleList);

  //User store methods
  rpc RequestIsInRole(UserRoleRequestByName) returns (UserIsInRoleReply);
  rpc RequestUserRole(UserRoleRequestById) returns (UserRole);
  rpc RequestUsersInRole(RoleRequestByName) returns (UserListReply);
  rpc RequestAddToRole(UserRoleRequestByName) returns (RequestResult);
  rpc RequestRemoveFromRole(UserRoleRequestByName) returns (RequestResult);
  rpc RequestGetRoles(UserRequestById) returns (RoleNameList);
}

message RoleRequestByName {
  string NormalizedName = 1;
}

message RoleRequestById {
  string RoleId = 1;
}

message UserRoleRequestByName {
  string UserId = 1;
  string NormalizedRoleName = 2;
}

message UserRoleRequestById {
  string UserId = 1;
  string RoleId = 2;
}

message UserIsInRoleReply {
  RequestResult RequestResult = 1;
  bool IsInRole = 2;
}

message UserRole {
  
  RequestResult Result = 1;
  /// <summary>
  /// Gets or sets the primary key of the user that is linked to a role.
  /// </summary>
  string UserId = 2;

  /// <summary>
  /// Gets or sets the primary key of the role that is linked to the user.
  /// </summary>
  string RoleId = 3;
}

message RoleNameList {
  repeated RoleNameRequest Roles = 1;
}

message RoleList {
  repeated RoleReply Roles = 1;
}

message RoleNameRequest {
  string Name = 1;
}

message RoleReply {
  /// <summary>
  /// Gets or sets the primary key for this role.
  /// </summary>
  string Id = 1;

  /// <summary>
  /// Gets or sets the name for this role.
  /// </summary>
  string Name = 2;

  /// <summary>
  /// Gets or sets the normalized name for this role.
  /// </summary>
  string NormalizedName = 3;

  /// <summary>
  /// A random value that should change whenever a role is persisted to the store
  /// </summary>
  string ConcurrencyStamp = 4;
  
  /// <summary>
  /// Gets or sets the RequestResult for this role.
  /// </summary>
  RequestResult RequestResult = 5;
}