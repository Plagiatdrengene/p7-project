﻿using System.Collections.Generic;
using System.Linq;
using Common.Data;
using Common.Protos.Course;
using Common.Protos.PhysicalQuestion;
using Common.Protos.Utility;
using Google.Protobuf.WellKnownTypes;

namespace Common.Utility {
	public static partial class RequestConversions {
		public static CourseReply ToCourseReply(this Course? course) {
			if (course?.CourseCode is not null)
				return new CourseReply {
					TinyCourse = course.ToTinyCourseReply(),
					Sessions = {course.Sessions.Select(ToSessionReply)},
					Participants = {course.Participants.Select(ToUserReplySafe)}
				};

			var result = new CourseReply {TinyCourse = new CourseTinyReply()};
			//TODO: Might not be the best place to do this
			result.TinyCourse.Result = RequestResult.FailRequest("Course was not found!");
			return result;
		}

		public static Course? ToCourse(this CourseReply? course) {
			if (course?.TinyCourse?.CourseCode is null) return null;

			if (!CheckForSuccess(course.TinyCourse.Result)) return null;
			var result = new Course(course.TinyCourse.CourseCode, course.TinyCourse.Name, course.TinyCourse.Year,
				course.TinyCourse.IsFall,
				course.TinyCourse.Creator.ToIdentityUser(),
				course.Participants.Select(ToIdentityUser).ToList(),
				course.Sessions.Select(ToSession).ToList(), course.TinyCourse.SubTitle);
			foreach (TagReply tagReply in course.TinyCourse.Tags) {
				var tag = tagReply.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					result.Tags.Add(tag);
				}
			}

			foreach (LanguageReply languageReply in course.TinyCourse.Languages) {
				var language = languageReply.ToLanguage();
				if (language != null && !string.IsNullOrEmpty(language.LanguageName)) {
					result.SupportedLanguages.Add(language);
				}
			}

			return null;
		}

		public static CourseTinyReply ToTinyCourseReply(this Course? course) {
			if (course?.CourseCode is not null)
				return new CourseTinyReply {
					Creator = course.Creator.ToUserReplySafe(),
					Languages = {
						course.SupportedLanguages.Select(ToLanguageReply)
					},
					Name = course.Name, Year = course.Year, CourseCode = course.CourseCode, IsFall = course.IsFall,
					SubTitle = course.SubTitle,
					Tags = {
						course.Tags.Select(ToTag)
					},
					Result = RequestResult.SucceededRequest()
				};

			//TODO: Might not be the best place to do this
			return new CourseTinyReply {Result = RequestResult.FailRequest("Course was not found!")};
		}

		public static CourseTinyReply ToTinyCourseReply(this TinyCourse? course) {
			if (course?.CourseCode is not null)
				return new CourseTinyReply {
					Creator = course.Creator.ToUserReplySafe(),
					Languages = {course.SupportedLanguages.Select(ToLanguageReply)},
					Name = course.Name, Year = course.Year, CourseCode = course.CourseCode, IsFall = course.IsFall,
					SubTitle = course.SubTitle,
					Tags = {course.Tags.Select(ToTag)}
				};
			//TODO: Might not be the best place to do this
			return new CourseTinyReply {Result = RequestResult.FailRequest("Course was not found!")};
		}

		public static TinyCourse? ToTinyCourse(this CourseTinyReply? course) {
			if (course is null) return null;

			if (!CheckForSuccess(course.Result)) return null;
			var tinyCourse = new TinyCourse(course.CourseCode, course.Name, course.Year, course.IsFall,
				course.Creator.ToIdentityUser(),
				course.SubTitle);
			foreach (TagReply tagReply in course.Tags) {
				var tag = tagReply.ToTag();
				if (tag != null && !string.IsNullOrEmpty(tag.TagName)) {
					tinyCourse.Tags.Add(tag);
				}
			}

			foreach (LanguageReply languageReply in course.Languages) {
				var language = languageReply.ToLanguage();
				if (language != null && !string.IsNullOrEmpty(language.LanguageName)) {
					tinyCourse.SupportedLanguages.Add(language);
				}
			}

			return tinyCourse;
		}
	}
}