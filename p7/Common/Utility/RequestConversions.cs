using System;
using System.Linq;
using System.Runtime.CompilerServices;
using Common.Data;
using Common.Protos.Course;
using Common.Protos.Session;
using Common.Protos.Utility;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Identity;

namespace Common.Utility {
	public static partial class RequestConversions {
		public static IdentityResult ToIdentityResult(this RequestResult requestResult) {
			if (requestResult.Succeeded) {
				return IdentityResult.Success;
			}

			IdentityError[] errors = requestResult.Errors.Select(error => new IdentityError {Description = error})
				.ToArray();
			return IdentityResult.Failed(errors);
		}

		public static RequestResult ToRequestResult(this IdentityResult identityResult) {
			return new RequestResult {
				Succeeded = identityResult.Succeeded,
				Errors = {identityResult.Errors.Select(error => error.Description)}
			};
		}

		public static SessionProto? ToSessionReply(this Session? session) {
			if (session?.Name is null) return null;
			return new SessionProto {
				SessionId = session.Id,
				Name = session.Name,
				StartDate = Timestamp.FromDateTime(session.StartDate.ToUniversalTime()),
				EndDate = Timestamp.FromDateTime(session.EndDate.ToUniversalTime())
			};
		}

		public static Session? ToSession(this SessionProto? session) {
			return session?.SessionId is null
				? null
				: new Session(session.Name, session.StartDate.ToDateTime().ToLocalTime(), session.EndDate.ToDateTime().ToLocalTime()) {Id = session.SessionId};
		}

		public static LanguageReply? ToLanguageReply(this Language? language) {
			return language?.LanguageName is null ? null : new LanguageReply {LanguageName = language.LanguageName};
		}

		public static Language? ToLanguage(this LanguageReply? language) {
			return language?.LanguageName is null ? null : new Language(language.LanguageName);
		}

		public static Tag? ToTag(this TagReply? tagReply) {
			return tagReply?.TagName is null ? null : new Tag(tagReply.TagName);
		}

		public static TagReply? ToTag(this Tag? tag) {
			return tag?.TagName is null ? null : new TagReply {TagName = tag.TagName};
		}

		public static bool CheckForSuccess(RequestResult result, [CallerMemberName] string caller = "") {
			if (result.Succeeded) return true;

			Console.Error.WriteLine($"{caller} FAILED:\n" + string.Join("\n", result.Errors));
			return false;
		}
	}
}