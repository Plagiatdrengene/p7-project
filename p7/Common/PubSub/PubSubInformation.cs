﻿namespace Common.PubSub {
	public class PubSubInformation {
		public PubSubInformation(string channelName, string clientId, string kubeMqServerAddress) {
			ChannelName = channelName;
			ClientId = clientId;
			KubeMqServerAddress = kubeMqServerAddress;
		}
		public string ChannelName { get; }
		public string ClientId { get; }
		public string KubeMqServerAddress { get; }
	}
}