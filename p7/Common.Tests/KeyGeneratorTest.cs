using System;
using System.Collections.Generic;
using Common.Utility;
using NUnit.Framework;

namespace Common.Tests {
	public class KeyGeneratorTest {
		[Test]
		public void TestKeyGenerator() {
			var codes = new HashSet<string>();

			for (var i = 0; i < 1000; i++) {
				var retries = 5;
				string code;

				do {
					code = KeyGenerator.RandomString(4);
					retries--;
				} while (codes.Contains(code) && retries != 0);


				Assert.False(codes.Contains(code));
				codes.Add(code);
			}

			Assert.Pass();
		}

		[Test]
		public void TestKeyGeneratorFail() {
			var codes = new HashSet<string>();

			for (var i = 0; i < 1000; i++) {
				string code = KeyGenerator.RandomString(1);
				codes.Add(code);
			}

			if (codes.Count != 1000)
				Assert.Pass();

			Assert.Fail("Some how there were no collisions.");
		}
	}
}